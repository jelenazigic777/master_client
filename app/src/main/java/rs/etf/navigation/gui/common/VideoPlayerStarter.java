package rs.etf.navigation.gui.common;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;

import rs.etf.navigation.place.Item;

import static rs.etf.navigation.gui.Constants.ITEM;

public class VideoPlayerStarter {

    protected static final String DEVELOPER_KEY = "AIzaSyDfwYGnOIYGGsyMg3mhUWBIN8it2S4dNXs";

    public static void play(Activity activity, Item item) {
        String video = item.getVideoUrl().toLowerCase();
        if (video.contains("youtube")) {
//            Intent intent = YouTubeStandalonePlayer.createVideoIntent(activity, DEVELOPER_KEY, item.getVideoUrl());
//            activity.startActivity(intent);
            activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(item.getVideoUrl())));
        } else {
            Intent intent = new Intent(activity, VideoSingleActivity.class);
            intent.putExtra(ITEM, item);
            activity.startActivity(intent);
        }
    }
}
