package rs.etf.navigation.gui.common;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class ImageLoader {

    public static void loadAsync(Activity activity, ImageView imageView, String url) {
        Glide.with(activity)
                .load(url) // Uri/String of the picture
                .into(imageView);
    }

    public static void loadAsync(View activity, ImageView imageView, String url) {
        Glide.with(activity)
                .load(url) // Uri/String of the picture
                .into(imageView);
    }

    public static void loadAsync(ImageView imageView, String url) {
        Glide.with(imageView.getContext())
                .load(url) // Uri/String of the picture
                .into(imageView);
        //new ImageAsyncTask(imageView).execute(url);
    }

    public static Bitmap load(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);

            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}