package rs.etf.navigation.gui.maps;

import android.os.Build;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;

public class MapShapeTouchListener implements View.OnTouchListener {

    private MapShape mapShape;
    private int _xDelta;
    private int _yDelta;

    public MapShapeTouchListener(MapShape mapShape) {
        this.mapShape = mapShape;
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        //TODO nedostaje uvecavanje i umanjivanje prikaza, za ovo se moze koristti kod klase TouchImageView

        final int X = (int) event.getRawX();
        final int Y = (int) event.getRawY();

        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                // _xDelta and _yDelta record how far inside the view we have touched. These
                // values are used to compute new margins when the view is moved.
                _xDelta = X - view.getLeft();
                _yDelta = Y - view.getTop();
                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_POINTER_DOWN:
            case MotionEvent.ACTION_POINTER_UP:
                // Do nothing
                break;
            case MotionEvent.ACTION_MOVE:
                RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) view
                        .getLayoutParams();
                // Image is centered to start, but we need to unhitch it to move it around.
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    lp.removeRule(RelativeLayout.CENTER_HORIZONTAL);
                    lp.removeRule(RelativeLayout.CENTER_VERTICAL);
                } else {
                    lp.addRule(RelativeLayout.CENTER_HORIZONTAL, 0);
                    lp.addRule(RelativeLayout.CENTER_VERTICAL, 0);
                }
                lp.leftMargin = X - _xDelta;
                lp.topMargin = Y - _yDelta;
                // Negative margins here ensure that we can move off the screen to the right
                // and on the bottom. Comment these lines out and you will see that
                // the image will be hemmed in on the right and bottom and will actually shrink.
                lp.rightMargin = view.getWidth() - lp.leftMargin - mapShape.getWindowWidth();
                lp.bottomMargin = view.getHeight() - lp.topMargin - mapShape.getWindowHeight();
                view.setLayoutParams(lp);
                break;
            default:
                break;
        }

        return true;
    }

}
