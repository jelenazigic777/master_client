package rs.etf.navigation.gui.exhibitions;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ListView;

import rs.etf.navigation.R;

import java.util.List;

import rs.etf.navigation.place.Item;

import static rs.etf.navigation.gui.Constants.EXHIBITIONS;
import static rs.etf.navigation.gui.Constants.ITEM;

public class ExhibitionsActivity extends ListActivity {

    private List<Item> items;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fullscreen();
        getListView().setBackgroundColor(getResources().getColor(R.color.backgroundColorT2));

        Bundle extras = getIntent().getExtras();
        this.items = (List<Item>) extras.getSerializable(EXHIBITIONS);

        ExhibitionsAdapter adapter = new ExhibitionsAdapter(this, items);
        setListAdapter(adapter);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Item item = (Item) getListAdapter().getItem(position);
        Intent intent = new Intent(ExhibitionsActivity.this, ExhibitionActivity.class);
        intent.putExtra(ITEM, item);
        startActivity(intent);
    }

    public void fullscreen() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }
}

