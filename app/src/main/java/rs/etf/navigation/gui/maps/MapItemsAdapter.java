package rs.etf.navigation.gui.maps;

import android.content.Context;
import android.content.res.Resources;
import android.os.strictmode.ResourceMismatchViolation;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import rs.etf.navigation.R;
import rs.etf.navigation.gui.common.ImageLoader;
import rs.etf.navigation.place.Entry;
import rs.etf.navigation.place.Item;
import rs.etf.navigation.place.MuseumMap;

public class MapItemsAdapter extends ArrayAdapter<Entry<Item, MuseumMap>> {

    Context context;
    List<Entry<Item, MuseumMap>> items;

    public MapItemsAdapter(Context context, List<Entry<Item, MuseumMap>> items) {
        super(context, R.layout.layout_map_item, items);
        this.context = context;
        this.items = items;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Entry<Item, MuseumMap> entry = items.get(position);
        Item item = entry.getKey();

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view;
        TextView textView;
        ImageView imageView;
        switch (item.getType()) {
            case Item.DIRECTION_LEFT:
            case Item.DIRECTION_RIGHT:
            case Item.ELEVATOR:
                view = inflater.inflate(R.layout.layout_map_direction, parent, false);

                textView = (TextView) view.findViewById(R.id.textTitle);
                textView.setText(item.getName());

                textView = (TextView) view.findViewById(R.id.textDescription);
                textView.setText(item.getText());

                //TODO staviti odgovarajuce slicice
                imageView = (ImageView) view.findViewById(R.id.image);

                ImageLoader.loadAsync(imageView, item.getImageUrl());
                break;
            case Item.ANNOUNCEMENT:
                view = inflater.inflate(R.layout.layout_map_audio, parent, false);

                textView = (TextView) view.findViewById(R.id.textTitle);
                textView.setText(item.getName());

                textView = (TextView) view.findViewById(R.id.textDescription);
                textView.setText(item.getText());
                break;
            case Item.ROOM:
                view = inflater.inflate(R.layout.layout_map_direction, parent, false);

                textView = (TextView) view.findViewById(R.id.textTitle);
                textView.setText(getContext().getString(R.string.got_to) + "\n" + item.getName());

                textView = (TextView) view.findViewById(R.id.textDescription);
                textView.setText(item.getText());

                imageView = (ImageView) view.findViewById(R.id.image);

                imageView.setImageResource(R.drawable.walk);

                break;
            case Item.STAIRCASE_UP:
                view = inflater.inflate(R.layout.layout_map_direction, parent, false);

                textView = (TextView) view.findViewById(R.id.textTitle);
                textView.setText(getContext().getString(R.string.staricase_up) );

                imageView = (ImageView) view.findViewById(R.id.image);

                imageView.setImageResource(R.drawable.walk_stairs_up);

                break;

            case Item.STAIRCASE_DOWN:
                view = inflater.inflate(R.layout.layout_map_direction, parent, false);

                textView = (TextView) view.findViewById(R.id.textTitle);
                textView.setText(getContext().getString(R.string.staricase_down));

                imageView = (ImageView) view.findViewById(R.id.image);

                imageView.setImageResource(R.drawable.walk_stairs_down);

                break;

            default:
                view = inflater.inflate(R.layout.layout_map_item, parent, false);
                imageView = (ImageView) view.findViewById(R.id.image);

                ImageLoader.loadAsync(imageView, item.getImageUrl());
                break;

        }

        return view;
    }

}
