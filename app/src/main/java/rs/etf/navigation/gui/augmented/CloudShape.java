package rs.etf.navigation.gui.augmented;


import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import rs.etf.navigation.R;
import rs.etf.navigation.place.Item;

/**
 * Created by emanu on 8/6/2018.
 */

public class CloudShape extends View {
//TODO ovo moze da se izbaci jer je realizacija napravljena koristeci XML

    public static final int LEFT = 50;
    public static final int RIGHT = 50;
    public static final int BOTTOM = 50;
    public static final int TOP = 50;
    public static final int CORNER_RADIUS = 12;
    public static final int TRIANGLE_Y1 = 26;
    public static final int TRIANGLE_X2 = 285;
    public static final int TRIANGLE_Y2 = 140;
    public static final int TRIANGLE_Y3 = 110;


    private int mHeight;
    private int mWidth;
    private Paint mPaint;
    private Path mPath;

    private TextView mTitleTextView, mAuthorTextView, mDescriptionTextView;

    public CloudShape(Context context, int height, int width) {
        super(context);
        this.mHeight = height;
        this.mWidth = width;

        init(null);
    }

    public CloudShape(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        init(attrs);
    }

    public CloudShape(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        init(attrs);
    }

    private void init(@Nullable AttributeSet set) {
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaint.setColor(getResources().getColor(R.color.backgroundColorT1));

        mPath = new Path();
    }

    @Override
    public void setVisibility(int visibility) {
        super.setVisibility(visibility);

        if (mTitleTextView != null) mTitleTextView.setVisibility(visibility);
        if (mAuthorTextView != null) mAuthorTextView.setVisibility(visibility);
        if (mDescriptionTextView != null) mDescriptionTextView.setVisibility(visibility);
    }

    public void onCreate(Activity context) {

        mTitleTextView = context.findViewById(R.id.textTitle);
        mAuthorTextView = context.findViewById(R.id.textAuthor);
        mDescriptionTextView = context.findViewById(R.id.textDescription);

        //todo napravi konstante za ove vrednosti
//        mDescriptionTextView.setWidth(getScreenHeight() - 80);
//        mDescriptionTextView.setTranslationX(getScreenWidth() / 2 - 105);
//        mTitleTextView.setTranslationX(getScreenWidth() / 2 - 225);
//        mAuthorTextView.setTranslationX(getScreenWidth() / 2 - 192);
//
//        //todo ako je veci od x line count dodaj 'read more'
//        int height = mDescriptionTextView.getHeight();
//        int lineCount = mDescriptionTextView.getLineCount();
//        //todo pomeri gornju ivicu opisa

    }

    private Paint createPaint(TextView textView) {
        Paint result = null;
        if (textView == null || textView.getPaint() == null) {
            result = new Paint(Paint.ANTI_ALIAS_FLAG);
            result.setColor(Color.parseColor("#EFEFEF"));
        } else {
            result = textView.getPaint();
        }

        return result;
    }

    public void setHeight(int height) {
        mHeight = height;
    }

    public void setWidth(int width) {
        mWidth = width;
    }

    @Override
    protected void onDraw(Canvas canvas) {

        int cHeight = canvas.getHeight();
        int cWidth = canvas.getWidth();
        float left = cHeight - BOTTOM - 250;
        float right = cHeight - BOTTOM;
        float top = RIGHT;
        float bottom = cWidth - LEFT;
        canvas.drawRoundRect(top, left, bottom, right, CORNER_RADIUS, CORNER_RADIUS, mPaint);

        float center = (top + bottom) / 2;
        mPath.reset();
        mPath.moveTo(center, left - TRIANGLE_Y1);
        mPath.lineTo(center - TRIANGLE_Y1, left);
        mPath.lineTo(center + TRIANGLE_Y1, left);
        canvas.drawPath(mPath, mPaint);

    }

    public void updateText(Item item) {
        if (mTitleTextView != null) mTitleTextView.setText(item.getName());
        if (mAuthorTextView != null) mAuthorTextView.setText(item.getAuthor());
        if (mDescriptionTextView != null) mDescriptionTextView.setText(item.getText());

    }
    //        mPaint.setShadowLayer(8, -10, 0, Color.GRAY);
//        setLayerType(LAYER_TYPE_SOFTWARE, null);

}
