package rs.etf.navigation.gui.maps;

import android.os.Bundle;

import rs.etf.navigation.R;
import rs.etf.navigation.db.DataProvider;
import rs.etf.navigation.gui.common.ImageLoader;
import rs.etf.navigation.gui.common.SimpleActivity;
import rs.etf.navigation.place.FlorMap;
import rs.etf.navigation.place.MuseumMap;

import static rs.etf.navigation.gui.Constants.MAP_ITEM;

public class MapActivity extends SimpleActivity {

    protected MuseumMap item;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        extractState(savedInstanceState);

        checkCenter();
    }

    public int getActivityLayout() {
        return R.layout.activity_map;
    }

    protected MuseumMap getNextItem() {
        Bundle extras = getIntent().getExtras();
        MuseumMap item = (MuseumMap) extras.getSerializable(MAP_ITEM);
        return item;
    }


    protected void extractState(Bundle savedInstanceState) {
        item = getNextItem();
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }


    protected void checkCenter() {
        if (item != null) {
            checkCenterImage();
        }
    }

    private void checkCenterImage() {
        FlorMap map = item.getFlorMap();
        if (map != null) {
            String url = map.getImageUrl();
            MapShape mapShape = findViewById(R.id.mapImage);
            DataProvider.getLocationProvider().registerLocationChangeListener(mapShape);
            mapShape.setMap(item);
            ImageLoader.loadAsync(mapShape, url);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        MapShape mapShape = findViewById(R.id.mapImage);
        if (mapShape != null) {
            DataProvider.getLocationProvider().unregisterLocationChangeListener(mapShape);
        }
    }

}
