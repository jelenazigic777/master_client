package rs.etf.navigation.gui.common;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import rs.etf.navigation.place.Item;

public class GalleryImageAdapter extends BaseAdapter {

    private Context context;
    private Item item;
    private int height;
    private int width;

    public GalleryImageAdapter(Context context, Item item, int height, int width) {
        this.context = context;
        this.item = item;
        this.height = height;
        this.width = width;
    }

    @Override
    public int getCount() {
        if (item != null && item.getAdditionalImagesUrl() != null)
            return item.getAdditionalImagesUrl().length;
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        if (convertView == null) {
            imageView = new ImageView(context);
            imageView.setLayoutParams(new GridView.LayoutParams(width / 2, width / 2));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
//            imageView.setPadding(8, 8, 8, 8);

        } else {
            imageView = (ImageView) convertView;
        }

        ImageLoader.loadAsync((Activity)context, imageView, item.getAdditionalImagesUrl()[position]);
        return imageView;
    }
}
