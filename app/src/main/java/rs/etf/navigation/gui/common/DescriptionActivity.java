package rs.etf.navigation.gui.common;

import android.app.*;
import android.os.*;
import android.util.*;
import android.view.*;
import android.webkit.*;

import rs.etf.navigation.R;

import rs.etf.navigation.place.Item;

import static rs.etf.navigation.gui.Constants.ITEM;

public class DescriptionActivity extends SimpleActivity {

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        progressDialog = ProgressDialog.show(this, getString(R.string.loading), getString(R.string.please_wait), true);
        progressDialog.setCancelable(false);

        WebView webView = findViewById(R.id.webView);
        webView.getSettings().setJavaScriptEnabled(true);

        Bundle extras = getIntent().getExtras();
        Item item = (Item) extras.getSerializable(ITEM);

        if (URLUtil.isValidUrl(item.getTextUrl())) {
            webView.getSettings().setLoadWithOverviewMode(true);
            webView.getSettings().setUseWideViewPort(true);
            webView.loadUrl(item.getTextUrl());
            webView.setWebViewClient(new WebViewClient() {

                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    progressDialog.show();
                    view.loadUrl(url);
                    return true;
                }

                @Override
                public void onPageFinished(WebView view, final String url) {
                    progressDialog.dismiss();
                }
            });
        } else {
            String htmlText = item.getTextUrl();
            String encodedHtml = Base64.encodeToString(htmlText.getBytes(), Base64.NO_PADDING);
            webView.loadData(encodedHtml, "text/html", "base64");
            progressDialog.dismiss();
        }

        //changeActivityLabelFont();
    }

    @Override
    public int getActivityLayout() {
        return R.layout.activity_description;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }

        return false;
    }

    /*
    public void changeActivityLabelFont(){
        Typeface typeface = ResourcesCompat.getFont(this, R.font.montserrat_medium);

        TextView textView = new TextView(this);
        textView.setText(TITLE);
        textView.setTypeface(typeface);
        textView.setTextSize(19);
        textView.setTextColor(Color.WHITE);

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayOptions(android.support.v7.app.ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setCustomView(textView);
        actionBar.setDisplayHomeAsUpEnabled(true);
    }
    */
}
