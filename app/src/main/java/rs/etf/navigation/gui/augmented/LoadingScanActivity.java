package rs.etf.navigation.gui.augmented;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import rs.etf.navigation.db.DataProvider;
import rs.etf.navigation.gui.common.ImageLoader;
import rs.etf.navigation.gui.common.SimpleActivity;
import rs.etf.navigation.place.Item;

import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.features2d.ORB;
import org.opencv.imgproc.Imgproc;

import java.util.*;

import rs.etf.navigation.R;
import rs.etf.navigation.place.Picture;

public class LoadingScanActivity extends SimpleActivity {
    //TODO promeniti da se ne skenira sve moguce objekte nego one koji se nalaze u neposrednoj blizini.

    private ORB mOrbDetector;
    private Object visit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (visit != null) {
            finish();
        }
        super.onCreate(savedInstanceState);

        if (OpenCVLoader.initDebug()) {
//            Toast.makeText(getApplicationContext(), "OpenCV successfully loaded", Toast.LENGTH_SHORT).show();
            Log.d("OPENCV", "OpenCV successfully loaded");
        } else {
//            Toast.makeText(getApplicationContext(), "OpenCV not loaded", Toast.LENGTH_SHORT).show();
            Log.d("OPENCV", "OpenCV not loaded");
        }

        mOrbDetector = ORB.create();

        getRestData();
    }

    @Override
    public int getActivityLayout() {
        return R.layout.activity_loading_scan;
    }

    public void getRestData() {
        new InitAsyncTask().execute();
    }

    public void initializeImageMatching() {
        //feature detection for each Picture object
        List<Picture> picturesList = new ArrayList<>();
        List<Item> items = DataProvider.getAllItems();
//TODO staviti da se ne ucitavaju sve slike na pocetku vec da se ucita nekoliko a da se ostatak ucitava kako ide kretanje kroz muzej
        for (Item item : items) {
            try {
                Picture currentPicture = new Picture(item);
                picturesList.add(currentPicture);

                Bitmap bitmap = ImageLoader.load(currentPicture.getImageUrl());

                Mat imgObject = new Mat();
                Utils.bitmapToMat(bitmap, imgObject);  //converting bitmap to Mat object
                Imgproc.cvtColor(imgObject, imgObject, Imgproc.COLOR_RGB2GRAY);  //converting to grayscale
                imgObject.convertTo(imgObject, 0);  //converting image type

                //creation of keypoints, descriptors and feature detection
                MatOfKeyPoint keypointsObject = new MatOfKeyPoint();
                Mat descriptorsObject = new Mat();
                mOrbDetector.detectAndCompute(imgObject, new Mat(), keypointsObject, descriptorsObject);

                currentPicture.setImgObject(imgObject);
                currentPicture.setKeypointsObject(keypointsObject);
                currentPicture.setDescriptorsObject(descriptorsObject);
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println(item.toString());
            }
        }
        DataProvider.setPictures(picturesList);
    }

    public void onLoad() {
        Intent intent = new Intent(this, ScanActivity.class);
        startActivity(intent);
        visit = new Object();
        finish();
    }

    private class InitAsyncTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            initializeImageMatching();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            onLoad();
        }
    }


}
