package rs.etf.navigation.gui.common;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ListView;

import rs.etf.navigation.R;

import java.util.*;

import rs.etf.navigation.place.Item;

import static rs.etf.navigation.gui.Constants.ITEM;
import static rs.etf.navigation.gui.Constants.VISITED_ITEMS;

public class VisitedItemsActivity extends ListActivity {

    private List<Item> items;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fullscreen();
        getListView().setBackgroundColor(getResources().getColor(R.color.backgroundColorT1));

        Bundle extras = getIntent().getExtras();
        this.items = (List<Item>) extras.getSerializable(VISITED_ITEMS);

        VisitedItemsAdapter adapter = new VisitedItemsAdapter(this, items);
        setListAdapter(adapter);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Item item = (Item) getListAdapter().getItem(position);
        Intent intent = new Intent(VisitedItemsActivity.this, ItemActivity.class);
        intent.putExtra(ITEM, item);
        startActivity(intent);
    }

    public void fullscreen() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }
}

