package rs.etf.navigation.gui.audio;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import rs.etf.navigation.db.DataProvider;
import rs.etf.navigation.location.Location;
import rs.etf.navigation.location.LocationChangeListener;
import rs.etf.navigation.place.Item;

public class AudioBuffer implements LocationChangeListener, Serializable {
    private List<Item> newItems;
    private List<Item> oldItems;

    public AudioBuffer() {
        newItems = new ArrayList<>();
        oldItems = new LinkedList<>();
    }

    @Override
    public void onLocationChange(Location location) {
        for (int i = newItems.size() - 1; i > 0; i--) {
            Item item = newItems.get(i);
            Location itemsLocation = item.getLocation();
            if (itemsLocation == null || itemsLocation.distance(location) > location.getWeight()) {
                newItems.remove(i);
            }
        }
        List<Item> closeItems = DataProvider.findItems(location);
        for (Item item : closeItems) {
            if (!oldItems.contains(item) && !newItems.contains(item) &&
                    (item.containsAudio() || item.containsAudioLocal())) {
                newItems.add(item);
            }
        }

    }

    public boolean haxNext() {
        return newItems.size() != 0 || (0 <= previous && previous < oldItems.size() - 1);
    }

    int previous = -1;

    public boolean haxPrevious() {
        return previous >= 0;
    }

    private Item lastItem;

    public Item getNextItem() {
        Item result;
        if (newItems.size() == 0) {
            if (previous >= oldItems.size()) {
                result = getAmbientAudio(lastItem);
            } else {
                result = oldItems.get(previous);
                previous++;
            }
        } else {
            result = newItems.remove(0);
            lastItem = result;
            oldItems.add(0, result);
            previous = oldItems.size() - 1;
        }
        return result;
    }

    public Item getPreviousItem() {
        Item result;
        if (oldItems.size() == 0 || previous < 0) {
            result = getAmbientAudio(lastItem);
        } else {
            result = oldItems.get(previous);
            previous--;
        }
        return result;
    }

    private Item getAmbientAudio(Item lastItem) {
        for (Item item : oldItems) {
            if (item.getAudioType() == Item.REPETITIVE_AUDIO) {
                return item;
            }
        }
        for (Item item : DataProvider.getAllItems()) {
            if (item.getAudioType() == Item.REPETITIVE_AUDIO) {
                return item;
            }
        }
        return null;
    }

    public void clear() {
        newItems.clear();
    }

    public void putItem(Item item) {
        newItems.clear();
        newItems.add(item);
    }
}
