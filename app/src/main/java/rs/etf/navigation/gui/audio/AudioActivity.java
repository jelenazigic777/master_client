package rs.etf.navigation.gui.audio;

import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import rs.etf.navigation.R;

import rs.etf.navigation.db.DataProvider;
import rs.etf.navigation.gui.common.AudioProgress;
import rs.etf.navigation.gui.common.ItemActivity;
import rs.etf.navigation.location.Location;
import rs.etf.navigation.location.LocationChangeListener;
import rs.etf.navigation.place.Item;

import static rs.etf.navigation.gui.Constants.ITEM_TO_PLAY;

public class AudioActivity extends ItemActivity implements LocationChangeListener, AudioProgress.AudioProgressInterface {
//TODO Proveriti da li radi sa promenom audio fokusa.
//TODO Kada se uredjaj okrene obezbediti da se nastavi od mesta gde se stalo.

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public int getActivityLayout() {
        return R.layout.activity_audio;
    }

    public static final int NOT_SET = -1;
    public static final int STOP = 0;
    public static final int PLAY = 1;
    public static final int PAUSE = 2;
    public static final String AUDIO_STATE = "AUDIO_STATE";
    protected int audioState;

    public static final String PASSED_TIME = "PASSED_TIME";
    protected int passedTime;

    protected MediaPlayer mediaPlayer;
    protected int duration;

    public static final String AUDIO_BUFFER = "AUDIO_BUFFER";
    protected AudioBuffer audioBuffer;

    public static final String AUTO_PLAY_MAP = "AUTO_PLAY_MAP";
    protected boolean autoPlayMap;

    @Override
    protected void extractState(Bundle savedInstanceState) {
        super.extractState(savedInstanceState);

        if (savedInstanceState != null) {
            audioState = savedInstanceState.getInt(AUDIO_STATE);
            audioBuffer = (AudioBuffer) savedInstanceState.getSerializable(AUDIO_BUFFER);
            autoPlayMap = savedInstanceState.getBoolean(AUTO_PLAY_MAP);
            passedTime = savedInstanceState.getInt(PASSED_TIME);
        } else {
            audioState = NOT_SET;
            audioBuffer = new AudioBuffer();
            autoPlayMap = false;
            passedTime = -1;
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt(AUDIO_STATE, audioState);
        outState.putSerializable(AUDIO_BUFFER, audioBuffer);
    }

    @Override
    protected void checkTop() {
        ImageButton audioText = (ImageButton) findViewById(R.id.audioText);
        audioText.setOnClickListener(new AdapterView.OnClickListener() {
            @Override
            public void onClick(View v) {
                actionText();
            }
        });

        ImageButton audioQR = (ImageButton) findViewById(R.id.audioQR);
        audioQR.setOnClickListener(new AdapterView.OnClickListener() {
            @Override
            public void onClick(View v) {
                actionQR();
            }
        });

        ImageButton audioMap = (ImageButton) findViewById(R.id.audioMap);
        audioMap.setOnClickListener(new AdapterView.OnClickListener() {
            @Override
            public void onClick(View v) {
                actionMapAutoPlay();
            }
        });
    }

    protected void actionMapAutoPlay() {
        autoPlayMap = !autoPlayMap;
        ImageButton mVideoButton = findViewById(R.id.audioMap);
        if (autoPlayMap) {
            mVideoButton.setImageResource(R.drawable.ic_location_on_black_24dp);
        } else {
            mVideoButton.setImageResource(R.drawable.ic_location_off_black_24dp);
        }
    }

    public static final int AUDIO_TEXT_REQUEST = 1;

    protected void actionText() {
        Intent intent = new Intent(this, AudioTextActivity.class);
        startActivityForResult(intent, AUDIO_TEXT_REQUEST);
    }

    public static final int AUDIO_QR_REQUEST = 2;

    protected void actionQR() {
        Intent intent = new Intent(this, AudioQRActivity.class);
        startActivityForResult(intent, AUDIO_QR_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case AUDIO_TEXT_REQUEST:
            case AUDIO_QR_REQUEST:
                if (resultCode == RESULT_OK) {
                    Item newItem = (Item) data.getSerializableExtra(ITEM_TO_PLAY);
                    if (newItem != null && newItem.containsAudio()) {
                        audioBuffer.putItem(newItem);
                        playNext();
                    }
                }
                break;
        }
    }

    private void play(Item item) {
        updateSelectedItem(item);
        stop();
        start();
    }

    private void playNext() {
        play(audioBuffer.getNextItem());
    }

    private void playPrevious() {
        play(audioBuffer.getPreviousItem());
    }

    @Override
    protected void checkCenterExtra() {
        if (showCenterExtra) {

        }
        checkAudioAction();
    }


    protected void checkAudioAction() {
        if (audioState == NOT_SET || audioState == STOP) {
            start();
        }

        ImageView button = (ImageView) findViewById(R.id.play_stop);
        button.setOnClickListener(new AdapterView.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (audioState) {
                    case PLAY:
                        pause();
                        break;
                    case PAUSE:
                        restart();
                        break;
                    case STOP:
                        actionText();
                        break;
                    default:
                        actionText();
                        break;
                }
            }
        });

        ImageView playPrevious = (ImageView) findViewById(R.id.playPrevious);
        playPrevious.setOnClickListener(new AdapterView.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (audioBuffer.haxPrevious()) {
                    playPrevious();
                } else {
                    if (audioState != PLAY) {
                        Toast toast = Toast.makeText(AudioActivity.this, getString(R.string.no_previous_audio), Toast.LENGTH_LONG);
                        toast.show();
                    }
                    stop();
                }
            }
        });

        ImageView playNext = (ImageView) findViewById(R.id.playNext);
        playNext.setOnClickListener(new AdapterView.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (audioBuffer.haxNext()) {
                    playNext();
                } else {
                    if (audioState != PLAY) {
                        Toast toast = Toast.makeText(AudioActivity.this, getString(R.string.no_next_audio), Toast.LENGTH_LONG);
                        toast.show();
                    }
                    stop();
                }
            }
        });
    }

    protected void stop() {
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
            passedTime = -1;
        }

        if (item != null) {
            audioState = STOP;
        } else {
            audioState = NOT_SET;
        }
        setPlayIcon();
        audioProgress.stopCountDownTimer();
    }

    protected void pause() {
        if (item != null) {
            if (audioState == PLAY) {
                try {
                    if (mediaPlayer.isPlaying()) {
                        passedTime = mediaPlayer.getCurrentPosition();
                        mediaPlayer.pause();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                stop();
            }
        }
        audioState = PAUSE;
        setPlayIcon();
    }

    protected void restart() {
        if (item != null) {
            if (audioState == PAUSE) {
                mediaPlayer.start();
                audioState = PLAY;
            } else {
                stop();
                audioState = STOP;
            }
        } else {
            audioState = STOP;
        }
        setPlayIcon();
    }

    protected void start() {
        if (item != null) {
            if (audioState != PLAY) {
                boolean pResult = play(item.getAudioUrl());
                audioProgress.startCountDownTimer();
                audioState = pResult ? PLAY : STOP;
            } else {
                if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                    audioState = PLAY;
                } else {
                    audioState = STOP;
                }
            }
        } else {
            audioState = NOT_SET;
        }
        setPlayIcon();
    }

    private void setPlayIcon() {
        ImageView button = (ImageView) findViewById(R.id.play_stop);
        switch (audioState) {
            case PLAY:
                button.setImageResource(R.drawable.ic_pause_circle_filled_black_24dp);
                break;
            case PAUSE:
                button.setImageResource(R.drawable.ic_play_circle_filled_black_24dp);
                break;
            default:
                button.setImageResource(R.drawable.ic_radio_button_checked_black_24dp);
                audioProgress.hideProgress();
                break;
        }
    }

    public boolean play(String url) {
        boolean result = true;
        try {
            if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                mediaPlayer.stop();
            }

            createMediaPlayer();

            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setDataSource(url);
            mediaPlayer.prepare();
            mediaPlayer.start();

            duration = mediaPlayer.getDuration();

        } catch (Exception e) {
            e.printStackTrace();
            result = false;
        }
        return result;

    }

    public void createMediaPlayer() {
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                if (audioBuffer.haxNext()) {
                    playNext();
                } else {
                    stop();
                }
            }
        });
        mediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                stop();
                Toast toast = Toast.makeText(AudioActivity.this, getString(R.string.audio_error), Toast.LENGTH_LONG);
                return false;
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        //TODO srediti okretanje ekrana
        DataProvider.getLocationProvider().registerLocationChangeListener(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        DataProvider.getLocationProvider().unregisterLocationChangeListener(this);
    }

    @Override
    public void finish() {
        if (mediaPlayer != null) {
            mediaPlayer.release();
        }
        super.finish();
    }

    @Override
    public void onLocationChange(Location location) {
        if (!autoPlayMap) return;
        audioBuffer.onLocationChange(location);
        if (audioState != PLAY && audioBuffer.haxNext()) {
            playNext();
        }
    }

    @Override
    protected void actionAudio(Item item) {
        stop();
        super.actionAudio(item);
    }

    @Override
    protected void actionVideo(Item item) {
        stop();
        super.actionVideo(item);
    }


    @Override
    public int getDuration() {
        return duration;
    }

    @Override
    public MediaPlayer getMediaPlayer() {
        return mediaPlayer;
    }

    AudioProgress audioProgress;

    @Override
    protected void extractLayout() {
        super.extractLayout();
        initViews();
    }

    private void initViews() {
        audioProgress = new AudioProgress(this, this);
    }
}