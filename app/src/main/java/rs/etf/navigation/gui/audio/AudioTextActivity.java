package rs.etf.navigation.gui.audio;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import rs.etf.navigation.R;

import rs.etf.navigation.db.*;
import rs.etf.navigation.gui.common.SimpleActivity;
import rs.etf.navigation.place.*;

import static rs.etf.navigation.gui.Constants.ITEM_TO_PLAY;

public class AudioTextActivity extends SimpleActivity {

    Item item;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        extractState(savedInstanceState);

        ImageView buttonPlay = (ImageView) findViewById(R.id.play);
        buttonPlay.setOnClickListener(new AdapterView.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText editText = (EditText) findViewById(R.id.itemIDText);
                String id = editText.getText().toString();
                item = DataProvider.getItem(id);
                if (item != null) {
                    if (item.containsAudio()) {
                        Intent intent = new Intent();
                        intent.putExtra(ITEM_TO_PLAY, item);
                        setResult(Activity.RESULT_OK, intent);
                        finish();
                    } else {
                        TextView title = (TextView) findViewById(R.id.textTitle);
                        title.setText(getString(R.string.item) + " " + id + " " + getString(R.string.no_audio));
                    }
                } else {
                    TextView title = (TextView) findViewById(R.id.textTitle);
                    title.setText(getString(R.string.item) + " " + id + " " + getString(R.string.does_not_exist));
                }
            }
        });

        EditText editText = (EditText) findViewById(R.id.itemIDText);
        editText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                    EditText editText = (EditText) findViewById(R.id.itemIDText);
                    String id = editText.getText().toString();
                    item = DataProvider.getItem(id);
                    if (item != null) {
                        checkDescriptions();
                    }
                }
                return false;
            }
        });
    }

    @Override
    public int getActivityLayout() {
        return R.layout.activity_audio_text;
    }

    public static final String SAVED_ID = "SAVED_ID";
    public static final String SAVED_ITEM = "SAVED_ITEM";

    protected void extractState(Bundle savedInstanceState) {
        String id;
        if (savedInstanceState != null) {
            id = savedInstanceState.getString(SAVED_ID);
            item = (Item) savedInstanceState.getSerializable(SAVED_ITEM);
        } else {
            id = "";
            item = null;
        }
        EditText editText = (EditText) findViewById(R.id.itemIDText);
        editText.setText(id);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        EditText editText = (EditText) findViewById(R.id.itemIDText);
        String id = editText.getText().toString();

        outState.putString(SAVED_ID, id);
        outState.putSerializable(SAVED_ITEM, item);
    }

    private void checkDescriptions() {
        if (item != null) {
            TextView textTitle = (TextView) findViewById(R.id.textTitle);
            textTitle.setText(item.getName());

            TextView authorTitle = (TextView) findViewById(R.id.textAuthor);
            authorTitle.setText(item.getAuthor());

            TextView textDescription = (TextView) findViewById(R.id.textDescription);
            textDescription.setText(item.getText());
        }
    }
}
