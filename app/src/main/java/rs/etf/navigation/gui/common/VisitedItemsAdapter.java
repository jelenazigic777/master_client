package rs.etf.navigation.gui.common;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import rs.etf.navigation.R;

import java.util.*;

import rs.etf.navigation.place.Item;

public class VisitedItemsAdapter extends ArrayAdapter<Item> {

    Context context;
    List<Item> items;

    public VisitedItemsAdapter(Context context, List<Item> items) {
        super(context, R.layout.layout_visited_item, items);
        this.context = context;
        this.items = items;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Item item = items.get(position);

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.layout_visited_item, parent, false);
        TextView textView = (TextView) view.findViewById(R.id.textViewItem);
        ImageView imageView = (ImageView) view.findViewById(R.id.icon);
        String text = item.getName() + ", " + item.getAuthor() + ", " + item.getText();
        textView.setText(text);
        ImageLoader.loadAsync(imageView, item.getImageUrl());
        return view;
    }
}
