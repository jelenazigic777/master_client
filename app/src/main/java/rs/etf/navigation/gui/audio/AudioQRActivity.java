package rs.etf.navigation.gui.audio;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.SparseArray;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import rs.etf.navigation.R;

import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;

import java.io.IOException;

import rs.etf.navigation.db.DataProvider;
import rs.etf.navigation.place.History;
import rs.etf.navigation.place.Item;

import static rs.etf.navigation.gui.Constants.CAMERA_PERMISSIONS_REQUEST;
import static rs.etf.navigation.gui.Constants.ITEM_TO_PLAY;

public class AudioQRActivity extends AppCompatActivity {
    protected Item item;
    protected String id;

    SurfaceView camPrev;
    BarcodeDetector detector;
    CameraSource cameraSource;

    History<String> history;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fullscreen();
        setContentView(R.layout.activity_audio_qr);
        getSupportActionBar().hide();

        extractState(savedInstanceState);
        checkDescriptions();

        ImageView buttonPlay = (ImageView) findViewById(R.id.play);
        buttonPlay.setOnClickListener(new AdapterView.OnClickListener() {
            @Override
            public void onClick(View v) {
                Item item = DataProvider.getItem(id);
                if (item != null) {
                    if (item.containsAudio()) {
                        Intent intent = new Intent();
                        intent.putExtra(ITEM_TO_PLAY, item);
                        setResult(Activity.RESULT_OK, intent);
                        finish();
                    } else {
                        TextView title = (TextView) findViewById(R.id.textTitle);
                        title.setText(getString(R.string.item) + " " + id + " " + getString(R.string.no_audio));
                    }
                } else {
                    TextView title = (TextView) findViewById(R.id.textTitle);
                    title.setText(getString(R.string.item) + " " + id + " " + getString(R.string.does_not_exist));
                }
            }
        });

        camPrev = findViewById(R.id.itemIDQR);
        detector = new BarcodeDetector.Builder(this).setBarcodeFormats(Barcode.QR_CODE).build();
        cameraSource = extractCamera();

        camPrev.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder surfaceHolder) {
                if (ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(AudioQRActivity.this, new String[]{android.Manifest.permission.CAMERA}, CAMERA_PERMISSIONS_REQUEST);
                    return;
                }

                try {
                    cameraSource.start(camPrev.getHolder());
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {

            }

            @Override
            public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
                cameraSource.stop();
            }
        });


        detector.setProcessor(new Detector.Processor<Barcode>() {

            @Override
            public void release() {
            }

            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {
                final SparseArray<Barcode> codes = detections.getDetectedItems();
                if (codes.size() != 0) {
                    String address = codes.valueAt(0).displayValue;
                    long now = System.currentTimeMillis();
                    if (history.check(address, now)) {
                        history.add(address, now);
                        camPrev.post(new Runnable() {
                            @Override
                            public void run() {
                                Vibrator vib = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                                vib.vibrate(500);
                                String address = codes.valueAt(0).displayValue;
                                item = DataProvider.getItemQR(address);
                                checkDescriptions();
                            }
                        });
                    }
                }
            }

        });

    }

    private CameraSource extractCamera() {
        boolean back = false;
        CameraManager manager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        try {
            for (String cameraId : manager.getCameraIdList()) {
                CameraCharacteristics chars = manager.getCameraCharacteristics(cameraId);
                Integer facing = chars.get(CameraCharacteristics.LENS_FACING);
                back =  back || facing == CameraMetadata.LENS_FACING_BACK;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (back) {
            return new CameraSource.Builder(this, detector).setRequestedPreviewSize(900, 480).setAutoFocusEnabled(true).build();
        } else {
            return new CameraSource.Builder(this, detector).setRequestedPreviewSize(900, 480).setAutoFocusEnabled(true).setFacing(CameraSource.CAMERA_FACING_FRONT).build();
        }
    }

    public static final String SAVED_ID = "SAVED_ID";
    public static final String SAVED_ITEM = "SAVED_ITEM";
    public static final String SAVED_HISTORY = "SAVED_HISTORY";

    protected void extractState(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            id = savedInstanceState.getString(SAVED_ID);
            item = (Item) savedInstanceState.getSerializable(SAVED_ITEM);
            history = (History<String>) savedInstanceState.getSerializable(SAVED_HISTORY);
        } else {
            id = "";
            item = null;
            history = new History<>();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString(SAVED_ID, id);
        outState.putSerializable(SAVED_ITEM, item);
        outState.putSerializable(SAVED_HISTORY, history);
    }

    private void checkDescriptions() {
        if (item != null) {
            TextView textTitle = (TextView) findViewById(R.id.textTitle);
            textTitle.setText(item.getName());

            TextView authorTitle = (TextView) findViewById(R.id.textAuthor);
            authorTitle.setText(item.getAuthor());

            TextView textDescription = (TextView) findViewById(R.id.textDescription);
            textDescription.setText(item.getText());
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case CAMERA_PERMISSIONS_REQUEST: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                        try {
                            cameraSource.start(camPrev.getHolder());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
                break;
            }
        }
    }

    public void fullscreen() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }
}