package rs.etf.navigation.gui.navigation;

import android.content.Intent;
import android.view.View;
import android.widget.ListView;

import rs.etf.navigation.gui.exhibitions.ExhibitionsActivity;
import rs.etf.navigation.place.Item;

import static rs.etf.navigation.gui.Constants.ITEM;

public class NavigationSelectActivity extends ExhibitionsActivity {

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Item item = (Item) getListAdapter().getItem(position);
        Intent intent = new Intent(NavigationSelectActivity.this, NavigationActivity.class);
        intent.putExtra(ITEM, item);
        startActivity(intent);
    }

}

