package rs.etf.navigation.gui.common;

import android.view.MotionEvent;
import android.view.View;

public class ImageTouchListener implements View.OnTouchListener {
    public static final int MOVE_THRESHOLD = 200;

    private ItemActivity itemShape;
    private int startX;
    private int startY;
    boolean done;

    public ImageTouchListener(ItemActivity itemShape) {
        this.itemShape = itemShape;
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        int x = (int) event.getRawX();
        int y = (int) event.getRawY();
        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                startX = x - view.getLeft();
                startY = y - view.getTop();
                done = false;
                break;
            case MotionEvent.ACTION_MOVE:
                if (!done && startX - x > MOVE_THRESHOLD && itemShape.activeLeftArrow()) {
                    startX = x;
                    startY = y;
                    done = true;
                    itemShape.actionLeftArrow();
                } else {
                    if (!done && x - startX > MOVE_THRESHOLD && itemShape.activeRightArrow()) {
                        startX = x;
                        startY = y;
                        done = true;
                        itemShape.actionRightArrow();
                    }
                }
                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_POINTER_DOWN:
            case MotionEvent.ACTION_POINTER_UP:
                break;
        }
        return false;
    }

}
