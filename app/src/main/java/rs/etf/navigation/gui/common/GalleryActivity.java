package rs.etf.navigation.gui.common;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import rs.etf.navigation.R;

import rs.etf.navigation.place.Item;

import static rs.etf.navigation.gui.Constants.ITEM;

public class GalleryActivity extends SimpleActivity {

    //private String mTitle;

    private GridView gridView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle extras = getIntent().getExtras();
        final Item item = (Item) extras.getSerializable(ITEM);

        //mTitle = item.getName();

        gridView = findViewById(R.id.gridView);
        gridView.setAdapter(new GalleryImageAdapter(this, item, getScreenHeight(), getScreenWidth()));
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(GalleryActivity.this, FullScreenImageActivity.class);
                Item data = item.copy();
                String [] additional = item.getAdditionalImagesUrl();
                if(additional != null && additional.length > position){
                    data.setImageUrl(additional[position]);
                }
                String [] additionalLocal = item.getAdditionalImagesUrlLocal();
                if(additionalLocal != null && additionalLocal.length > position){
                    data.setImageUrlLocal(additionalLocal[position]);
                }
                intent.putExtra(ITEM, data);
                startActivity(intent);
            }
        });

        //changeActivityLabelFont();
    }

    @Override
    public int getActivityLayout() {
        return R.layout.activity_gallery;
    }

    public static int getScreenWidth(){
        return Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    public static int getScreenHeight(){
        return Resources.getSystem().getDisplayMetrics().heightPixels;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                this.finish();
                return true;
        }

        return false;
    }

    /*
    public void changeActivityLabelFont(){
        Typeface typeface = ResourcesCompat.getFont(this, R.font.montserrat_medium);

        TextView textView = new TextView(this);
        textView.setText(mTitle);
        textView.setTypeface(typeface);
        textView.setTextSize(19);
        textView.setTextColor(Color.WHITE);

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayOptions(android.support.v7.app.ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setCustomView(textView);
        actionBar.setDisplayHomeAsUpEnabled(true);
    }
    */
}
