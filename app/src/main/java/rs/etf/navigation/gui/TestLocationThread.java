package rs.etf.navigation.gui;

import android.content.Context;
import android.content.Intent;

import java.util.List;

import rs.etf.navigation.db.DataProvider;
import rs.etf.navigation.location.Location;
import rs.etf.navigation.place.Item;

public class TestLocationThread extends Thread {
    Context context;

    public TestLocationThread(Context context) {
        this.context = context;
    }

    static final int MAXX = 25, MAXY = 10;
    static boolean started = false;

    public void run() {
        if (started) return;
        started = true;
        float lastX = (float) (MAXX * Math.random()), lastY = (int) (MAXY * Math.random());
        List<Item> items = DataProvider.getAllItems();
        int j = (int) (Math.random() * items.size());
        for (int i = 0; i < 1000; i++) {
            Intent intent = new Intent();
            intent.setAction(Constants.LOCATION_INTENT);
            Location l = items.get(j).getLocation();
            lastX = move(lastX, MAXX, (int) l.getX());
            lastY = move(lastY, MAXY, (int) l.getY());

            Location location = new Location(lastX, lastY, 200);
            location.setWeight(1 + Math.random());
            location.setViewingAngle(360 * Math.random());
            double distance = Math.sqrt(Math.pow(lastX - l.getX(), 2) + Math.pow(lastY - l.getY(), 2));
            if (distance < location.getWeight()) {
                j = (j + 1) % items.size();
            }

            intent.putExtra(Constants.LOCATION, location);

            context.sendBroadcast(intent);
            try {
                this.sleep(2000);
            } catch (Exception e) {
            }
        }
    }

    static float move(float from, float max, float to) {
        float result;
        if (from < to) {
            result = from + (float) (2 * Math.random());
        } else {
            result = from - (float) (2 * Math.random());
        }
        if (result < 0) result = 0;
        else if (result > max) result = max;
        return result;
    }

}
