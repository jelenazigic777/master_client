package rs.etf.navigation.gui.common;

import android.os.Bundle;
import android.widget.ImageView;

import rs.etf.navigation.R;

import rs.etf.navigation.place.Item;

import static rs.etf.navigation.gui.Constants.ITEM;

public class FullScreenImageActivity extends SimpleActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle extras = getIntent().getExtras();
        Item item = (Item) extras.getSerializable(ITEM);
        String url = item.getImageUrl();

        ImageView imageView = findViewById(R.id.fullScreenImageView);

        ImageLoader.loadAsync(this, imageView, url);
    }

    @Override
    public int getActivityLayout() {
        return R.layout.activity_full_screen_picture;
    }
}
