package rs.etf.navigation.gui.augmented;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceView;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import java.util.List;

import rs.etf.navigation.R;
import rs.etf.navigation.db.DataProvider;
import rs.etf.navigation.gui.common.ItemActivity;
import rs.etf.navigation.gui.common.VideoPlayerStarter;
import rs.etf.navigation.place.History;
import rs.etf.navigation.place.Item;
import rs.etf.navigation.place.Picture;

import static rs.etf.navigation.gui.Constants.CAMERA_PERMISSIONS_REQUEST;
import static rs.etf.navigation.gui.Constants.ITEM;

public class ScanActivity extends ItemActivity implements CameraBridgeViewBase.CvCameraViewListener2 {

    private JavaCameraViewAugmented mJavaCameraView;
    private Mat mRgba; //red green blue alpha
    private Mat mGray;
    private Mat mResult;
    private Comparator comparator;

    private ImageButton mVideoButton;
    private ImageButton mAudioButton;

    private List<Picture> mPicturesList;

    private int counterPass;
    private float[] sceneCornersData;
    private Scalar mColor;
    private boolean process = true;

    private int mFrameCnt = 1;
    private int mPictureCnt = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public int getActivityLayout() {
        return R.layout.activity_augmented;
    }

    @Override
    protected Item getNextItem() {
        Item item = null;
        mMap = null;
        return item;
    }

    @Override
    protected void checkTop() {
        mVideoButton = findViewById(R.id.videoAutoPlay);
        mAudioButton = findViewById(R.id.audioAutoPlay);
    }

    @Override
    protected void checkBottom() {
        View mBubbleShape = findViewById(R.id.bubbleShape);
        mBubbleShape.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (item != null) {
                    Intent intent = new Intent(ScanActivity.this, ItemActivity.class);
                    intent.putExtra(ITEM, item);
                    startActivity(intent);
                }
            }
        });
    }

    @Override
    protected void checkBottomExtra() {
        // razlicito
    }

    @Override
    protected void checkCenter() {
        mJavaCameraView = findViewById(R.id.javaCameraView);
        mJavaCameraView.setVisibility(SurfaceView.VISIBLE);
        mJavaCameraView.setCvCameraViewListener(this);

        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, CAMERA_PERMISSIONS_REQUEST);
        }

        if (OpenCVLoader.initDebug()) {
//            Toast.makeText(getApplicationContext(), "OpenCV successfully loaded", Toast.LENGTH_SHORT).show();
            Log.d("OPENCV", "OpenCV successfully loaded");
        } else {
//            Toast.makeText(getApplicationContext(), "OpenCV not loaded", Toast.LENGTH_SHORT).show();
            Log.d("OPENCV", "OpenCV not loaded");
        }

        if (item != null) {
            checkTopArrow();
        }
    }

    protected static final String AUTO_PLAY_VIDEO = "AUTO_PLAY_VIDEO";
    protected static final String AUTO_PLAY_AUDIO = "AUTO_PLAY_AUDIO";
    protected boolean autoPlayVideo, autoPlayAudio;

    public static final String SAVED_AUDIO_HISTORY = "SAVED_AUDIO_HISTORY";
    public static final String SAVED_VIDEO_HISTORY = "SAVED_VIDEO_HISTORY";
    History<Item> videoHistory, audioHistory;

    @Override
    protected void extractState(Bundle savedInstanceState) {
        super.extractState(savedInstanceState);
        comparator = new Comparator();
        mPicturesList = DataProvider.getPictures();//LoadingScanActivity.sPicturesList;

        if (savedInstanceState != null) {
            autoPlayVideo = savedInstanceState.getBoolean(AUTO_PLAY_VIDEO);
            autoPlayAudio = savedInstanceState.getBoolean(AUTO_PLAY_AUDIO);
            audioHistory = (History<Item>) savedInstanceState.getSerializable(SAVED_AUDIO_HISTORY);
            videoHistory = (History<Item>) savedInstanceState.getSerializable(SAVED_VIDEO_HISTORY);
        } else {
            autoPlayVideo = false;
            autoPlayAudio = false;
            audioHistory = new History<>();
            videoHistory = new History<>();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(AUTO_PLAY_VIDEO, autoPlayVideo);
        outState.putBoolean(AUTO_PLAY_AUDIO, autoPlayAudio);
        outState.putSerializable(SAVED_AUDIO_HISTORY, audioHistory);
        outState.putSerializable(SAVED_VIDEO_HISTORY, videoHistory);
    }


    @Override
    public void onCameraViewStarted(int width, int height) {
        mRgba = new Mat(height, width, CvType.CV_8UC4);
        mGray = new Mat(height, width, CvType.CV_8UC1);

        mColor = new Scalar(0, 206, 255);

        createTmpMap(width, height);
        comparator.init();

    }

    private void createTmpMap(int width, int height) {
        int screenWidth = mJavaCameraView.getWidth();//getScreenWidth();
        int screenHeight = mJavaCameraView.getHeight();//getScreenHeight();

        float scale1 = ((float) Math.max(width, height)) / Math.min(width, height);

        int imageWidth = (int) Math.min(screenWidth, scale1 * screenHeight);
        int imageHeight = (int) Math.min(screenHeight, scale1 * screenWidth);

        mResult = new Mat(imageHeight, imageWidth, CvType.CV_8UC4);

        mJavaCameraView.check(imageWidth, imageHeight);
    }

    @Override
    public void onCameraViewStopped() {
        mRgba.release();
        mGray.release();
        mResult.release();
    }

    @Override
    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
        mRgba = inputFrame.rgba();

        Imgproc.cvtColor(mRgba, mGray, Imgproc.COLOR_RGB2GRAY);

        //frame count govori da li se svaki drugi ili svaki treci cetvrti frame obradjuje
        Mat ret = null;
        if (counterPass == mFrameCnt) {
            ret = match();
            counterPass = 0;
        } else {
            counterPass++;
            ret = mRgba;
            //drawCorners(ret);
        }

        ret = adjust(ret);
        return ret;
    }

    protected Mat adjust(Mat data) {
        if (data != null) {
            Mat mRgbaT = null;
            int rotation = getWindowManager().getDefaultDisplay().getRotation();
            switch (rotation) {
                case Surface.ROTATION_0:
                    // Rotate clockwise 90 degrees
                    mRgbaT = data.t();
                    Core.flip(mRgba.t(), mRgbaT, 1);
                    break;
                case Surface.ROTATION_90:
                    mRgbaT = data;
                    break;
                case Surface.ROTATION_180:
                    // Rotate clockwise 90 degrees
                    mRgbaT = data.t();
                    Core.flip(mRgba.t(), mRgbaT, 0);
                    break;
                case Surface.ROTATION_270://??
                    mRgbaT = data.t();
                    Core.flip(mRgba.t(), mRgbaT, -1);
                    break;
            }
            // Resize
            Imgproc.resize(mRgbaT, mResult, mResult.size());
            data = mResult;
        }
        return data;
    }

    public Mat match() {
        if (mPicturesList == null)
            return mRgba;

        if (!process)
            return mRgba;

        comparator.set(mGray);
        if (comparator.isEmpty()) {
            return mRgba;
        }

        for (int j = 0; j < mPicturesList.size(); j++) {
            final Picture picture = mPicturesList.get((mPictureCnt + j) % mPicturesList.size());
            boolean comparision = comparator.compare(picture);
            if (comparision) {
                mPictureCnt = (mPictureCnt + j) % mPicturesList.size();
                //mCurrentContent = findLanguage(language);
                item = picture;
                mMap = DataProvider.getMap(item);
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showContent(item);
                    }
                });
                return mRgba;
            }
        }

        //if for loop is over that means none pictures have been matched
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideContent();
            }
        });

        return mRgba;
    }

    protected Mat drawCorners(Mat mat) {
        sceneCornersData = comparator.getCorners();
        if (sceneCornersData != null) {
            //drawing lines between corners
            Imgproc.line(mat, new Point(sceneCornersData[0], sceneCornersData[1]),
                    new Point(sceneCornersData[2], sceneCornersData[3]), mColor, 4);
            Imgproc.line(mat, new Point(sceneCornersData[2], sceneCornersData[3]),
                    new Point(sceneCornersData[4], sceneCornersData[5]), mColor, 4);
            Imgproc.line(mat, new Point(sceneCornersData[4], sceneCornersData[5]),
                    new Point(sceneCornersData[6], sceneCornersData[7]), mColor, 4);
            Imgproc.line(mat, new Point(sceneCornersData[6], sceneCornersData[7]),
                    new Point(sceneCornersData[0], sceneCornersData[1]), mColor, 4);
        }
        return mat;
    }

    /*
        public Item findLanguage(String language) {
            for (Item content : mCurrentPicture.getContentList()) {
                if (content.getLanguage().equalsIgnoreCase(language))
                    return content;
            }

            return mCurrentPicture.getContentList().get(0); //nulti je podrazumevani
        }
    */
    public void hideContent() {
        View mBubbleShape = findViewById(R.id.bubbleShape);
        mBubbleShape.setVisibility(View.GONE);

        sceneCornersData = null;

        //ubrzaj
        mFrameCnt = 3;
    }

    public void showContent(Item item) {

        checkCenter();

        TextView mTitleTextView = findViewById(R.id.textTitle);
        if (mTitleTextView != null) mTitleTextView.setText(item.getName());
        TextView mAuthorTextView = findViewById(R.id.textAuthor);
        if (mAuthorTextView != null) mAuthorTextView.setText(item.getAuthor());
        TextView mDescriptionTextView = findViewById(R.id.textDescription);
        if (mDescriptionTextView != null) mDescriptionTextView.setText(item.getText());

        View mBoubbleShape = findViewById(R.id.bubbleShape);
        mBoubbleShape.setVisibility(View.VISIBLE);

        //smanji obradu
        mFrameCnt = 6;

        long currentTime = System.currentTimeMillis();
        if (autoPlayAudio && audioHistory.check(item, currentTime)) {
            audioHistory.add(item, currentTime);
            actionAudio(item);
        } else {
            if (autoPlayVideo && videoHistory.check(item, currentTime)) {
                videoHistory.add(item, currentTime);
                actionVideo(item);
            }
        }
    }


    public void onClickVideoButton(View view) {
        autoPlayVideo = !autoPlayVideo;
        if (autoPlayVideo) {
            mVideoButton.setImageResource(R.drawable.ic_videocam_black_24dp);
        } else {
            mVideoButton.setImageResource(R.drawable.ic_videocam_off_black_24dp);
        }
    }

    protected void actionVideo(Item item) {
        VideoPlayerStarter.play(this, item);
    }

    protected boolean isActiveVideo(Item item) {
        boolean result = false;
        if (item != null && item.containsVideo()) {
            result = true;
        }
        return result;
    }

    public void onClickAudioButton(View view) {
        autoPlayAudio = !autoPlayAudio;
        if (autoPlayAudio) {
            mAudioButton.setImageResource(R.drawable.ic_volume_up_black_24dp);
        } else {
            mAudioButton.setImageResource(R.drawable.ic_volume_off_black_24dp);
        }
    }

    public void onClickSearchButton(View view) {
        showDialog();
        process = false;
    }

    public void onClickCancelButton(View view) {
        hideContent();
        process = true;
    }

    public void showDialog() {
        final EditText editText = new EditText(this);
        editText.setInputType(InputType.TYPE_CLASS_NUMBER);
        editText.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        lp.setMargins(50, 5, 50, 10);
        editText.setLayoutParams(lp);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(editText);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
//                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                onInputRegisterNumber(editText.getText().toString());
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.setMessage(R.string.input_register_number);
        builder.create().show();
    }

    public void onInputRegisterNumber(String id) {
        Picture picture = DataProvider.getPicture(id);
        if (picture != null) {
            item = picture;
            mMap = DataProvider.getMap(item);
            showContent(item);
            Intent intent = new Intent(ScanActivity.this, ItemActivity.class);
            intent.putExtra(ITEM, item);
            startActivity(intent);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mJavaCameraView != null)
            mJavaCameraView.disableView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mJavaCameraView != null)
            mJavaCameraView.disableView();

    }

    @Override
    protected void onResume() {
        super.onResume();

        if (OpenCVLoader.initDebug()) {
            Log.d("OPENCV", "OpenCV successfully loaded");
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        } else {
            Log.d("OPENCV", "OpenCV not loaded");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_4_0, this, mLoaderCallback);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case CAMERA_PERMISSIONS_REQUEST: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                } else {
                    // permission denied
                    Toast.makeText(ScanActivity.this, getString(R.string.permission_denied_camera), Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }

    public static int getScreenWidth() {
        return Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    public static int getScreenHeight() {
        return Resources.getSystem().getDisplayMetrics().heightPixels;
    }


    @Override
    public void onBackPressed() {
//        Intent intent = new Intent(this, HomeActivity.class);
//        startActivity(intent);
// TODO proveriti !!ovo stoji jer prethodna aktivnost ovoj aktivnosti bila LoadingScanActivity
        finish();
    }

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case BaseLoaderCallback.SUCCESS:
                    mJavaCameraView.enableView();
                    break;
                default:
//                    super.onManagerConnected(status);
                    break;
            }
            super.onManagerConnected(status);
        }
    };

}
//TODO ako je uneta neka slika i ona se obisla onda ostaje njen proyor oblacic kako da je ona upravo prepoznata.