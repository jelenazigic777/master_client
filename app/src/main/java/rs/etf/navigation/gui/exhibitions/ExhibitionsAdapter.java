package rs.etf.navigation.gui.exhibitions;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import rs.etf.navigation.R;

import java.util.List;

import rs.etf.navigation.gui.common.ImageLoader;
import rs.etf.navigation.place.Item;

public class ExhibitionsAdapter extends ArrayAdapter<Item> {

    Context context;
    List<Item> items;

    public ExhibitionsAdapter(Context context, List<Item> items) {
        super(context, R.layout.layout_exibition_item, items);
        this.context = context;
        this.items = items;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Item item = items.get(position);

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = null;
        String[] images = getImages(item);
        if (images == null || images.length < 2) {
            view = inflater.inflate(R.layout.layout_exibition_item, parent, false);
        } else {
            view = inflater.inflate(R.layout.layout_exibition_item3, parent, false);
        }
        TextView textView = (TextView) view.findViewById(R.id.textTitle);
        textView.setText(item.getName());

        textView = (TextView) view.findViewById(R.id.textAuthor);
        textView.setText(item.getAuthor());

        textView = (TextView) view.findViewById(R.id.textDescription);
        textView.setText(item.getText());

        ImageView imageView = (ImageView) view.findViewById(R.id.image);

        ImageLoader.loadAsync(imageView, item.getImageUrl());

        if (images != null && images.length >= 2) {
            ImageView imageView2 = (ImageView) view.findViewById(R.id.image2);
            ImageLoader.loadAsync(imageView2, imageView2, images[0]);
            ImageView imageView3 = (ImageView) view.findViewById(R.id.image3);
            ImageLoader.loadAsync(imageView3, imageView3, images[1]);
        }
        return view;
    }

    private String[] getImages(Item item) {
        return item.getAdditionalImagesUrl();
    }
}
