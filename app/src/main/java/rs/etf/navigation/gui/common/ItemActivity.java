package rs.etf.navigation.gui.common;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import rs.etf.navigation.R;

import rs.etf.navigation.db.DataProvider;
import rs.etf.navigation.gui.maps.MapShape;
import rs.etf.navigation.place.FlorMap;
import rs.etf.navigation.place.Item;
import rs.etf.navigation.place.MuseumMap;

import static rs.etf.navigation.gui.Constants.ITEM;

public class ItemActivity extends SimpleActivity {

    protected Item item;
    protected MuseumMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        extractState(savedInstanceState);

        checkTop();
        checkCenter();
        checkBottom();

        checkExtra();

    }

    @Override
    public int getActivityLayout() {
        return R.layout.activity_item;
    }

    protected Item getNextItem() {
        Bundle extras = getIntent().getExtras();
        Item item;
        if (extras != null) {
            item = (Item) extras.getSerializable(ITEM);
        } else {
            item = null;
        }
        return item;
    }


    protected MuseumMap getMapItem(Item item) {
        if (item == null) return null;
        MuseumMap mMap = DataProvider.getMap(item);
        return mMap;
    }

    protected static final String IMAGE_POSITION = "IMAGE_POSITION";
    protected static final String SHOW_TOP_EXTRA = "SHOW_TOP_EXTRA";
    protected static final String SHOW_CENTER_EXTRA = "SHOW_CENTER_EXTRA";
    protected static final String SHOW_BOTTOM_EXTRA = "SHOW_BOTTOM_EXTRA";
    protected static final String LANGUAGE = "LANGUAGE";
    protected int imagePosition;
    protected boolean showTopExtra, showCenterExtra, showBottomExtra;
    protected String language;

    protected void extractState(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            imagePosition = savedInstanceState.getInt(IMAGE_POSITION);
            showTopExtra = savedInstanceState.getBoolean(SHOW_TOP_EXTRA);
            showCenterExtra = savedInstanceState.getBoolean(SHOW_CENTER_EXTRA);
            showBottomExtra = savedInstanceState.getBoolean(SHOW_BOTTOM_EXTRA);
            language = savedInstanceState.getString(LANGUAGE);
        } else {
            imagePosition = 0;
            showTopExtra = false;
            showCenterExtra = false;
            showBottomExtra = false;
            language = "en";
        }
        updateItem(getNextItem());
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt(IMAGE_POSITION, imagePosition);
        outState.putBoolean(SHOW_TOP_EXTRA, showTopExtra);
        outState.putBoolean(SHOW_CENTER_EXTRA, showCenterExtra);
        outState.putBoolean(SHOW_BOTTOM_EXTRA, showBottomExtra);
        outState.putString(LANGUAGE, language);
    }

    protected void checkTop() {

    }

    protected void checkCenter() {
        if (item != null) {
            checkCenterImage();

            checkLeftArrow();
            checkRightArrow();
            checkTopArrow();
            checkBottomArrow();
        }

    }

    protected void checkCenterImage() {
        String url = item.getImageUrl();
        ImageView imageView = findViewById(R.id.centralImage);
        imageView.setOnTouchListener(new ImageTouchListener(this));
        ImageLoader.loadAsync(this, imageView, url);
    }

    protected void checkBottom() {
        if (item != null) {
            TextView textTitle = (TextView) findViewById(R.id.textTitle);
            textTitle.setText(item.getName());

            TextView authorTitle = (TextView) findViewById(R.id.textAuthor);
            authorTitle.setText(item.getAuthor());

            TextView textDescription = (TextView) findViewById(R.id.textDescription);
            textDescription.setText(item.getText());
        }
    }


    protected void checkBottomExtra() {
        LinearLayout layout = (LinearLayout) findViewById(R.id.additionalBottomLayout);
        layout.removeAllViews();
        if (showBottomExtra) {
            checkDetails(layout);
            checkAudio(layout);
            checkVideo(layout);
            checkGallery(layout);
        }
    }

    protected void checkDetails(LinearLayout layout) {
        if (isActiveDetails(item)) {
            View view = createDetailElementView(layout, getString(R.string.details), R.drawable.ic_photo_black_24dp);
            view.setOnClickListener(new AdapterView.OnClickListener() {
                @Override
                public void onClick(View v) {
                    actionDetails(item);
                }
            });
        }
    }

    @NonNull
    protected View createDetailElementView(LinearLayout layout, String title, int icon) {
        LayoutInflater inflater = (LayoutInflater) layout.getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.layout_additiona_bottom_element, layout, false);

        TextView textView = (TextView) view.findViewById(R.id.title);
        textView.setText(title);

        ImageView imageView = (ImageView) view.findViewById(R.id.icon);
        imageView.setImageResource(icon);

        layout.addView(view);
        return view;
    }

    protected void actionDetails(Item item) {
        Intent intent = new Intent(this, DescriptionActivity.class);
        intent.putExtra(ITEM, item);
        startActivity(intent);
    }

    protected boolean isActiveDetails(Item item) {
        boolean result = false;
        if (item != null && item.containsDetails()) {
            result = true;
        }
        return result;
    }

    protected void checkAudio(LinearLayout layout) {
        if (isActiveAudio(item)) {
            View view = createDetailElementView(layout, getString(R.string.audio), R.drawable.ic_volume_down_black_24dp);
            view.setOnClickListener(new AdapterView.OnClickListener() {
                @Override
                public void onClick(View v) {
                    actionAudio(item);
                }
            });
        }
    }

    protected void actionAudio(Item item) {
        Intent intent = new Intent(this, AudioSingleActivity.class);
        intent.putExtra(ITEM, item);
        startActivity(intent);
    }

    protected boolean isActiveAudio(Item item) {
        boolean result = false;
        if (item != null && item.containsAudio()) {
            result = true;
        }
        return result;
    }

    protected void checkVideo(LinearLayout layout) {
        if (isActiveVideo(item)) {
            View view = createDetailElementView(layout, getString(R.string.video), R.drawable.ic_videocam_black_24dp);
            view.setOnClickListener(new AdapterView.OnClickListener() {
                @Override
                public void onClick(View v) {
                    actionVideo(item);
                }
            });
        }
    }


    protected void actionVideo(Item item) {
        VideoPlayerStarter.play(this, item);
    }

    protected boolean isActiveVideo(Item item) {
        boolean result = false;
        if (item != null && item.containsVideo()) {
            result = true;
        }
        return result;
    }

    protected void checkGallery(LinearLayout layout) {
        if (isActiveGallery(item)) {
            View view = createDetailElementView(layout, getString(R.string.gallery), R.drawable.ic_photo_album_black_24dp);
            view.setOnClickListener(new AdapterView.OnClickListener() {
                @Override
                public void onClick(View v) {
                    actionGallery(item);
                }
            });
        }
    }

    protected void actionGallery(Item item) {
        Intent intent = new Intent(this, GalleryActivity.class);
        intent.putExtra(ITEM, item);
        startActivity(intent);
    }

    protected boolean isActiveGallery(Item item) {
        boolean result = false;
        if (item != null && item.containsAdditionalImages()) {
            result = true;
        }
        return result;
    }

    protected void checkCenterExtra() {
        if (showCenterExtra) {

        } else {

        }
    }

    protected void checkTopExtra() {
        LinearLayout layout = (LinearLayout) findViewById(R.id.additionalTopLayout);
        layout.removeAllViews();
        if (showTopExtra && mMap != null && mMap.containsContent()) {
            LayoutInflater inflater = (LayoutInflater) layout.getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.layout_map_top, layout, false);
            view.setVisibility(View.VISIBLE);
            layout.addView(view);

            fillMap();
        }

    }

    private void fillMap() {
        MapShape mapShape = findViewById(R.id.mapImage);
        if (mapShape == null) return;

        DataProvider.getLocationProvider().registerLocationChangeListener(mapShape);
        mapShape.setSelectedItem(item);
        if (mapShape.getMap() != mMap) {
            mapShape.setMap(mMap);

            ViewGroup rootLayout = findViewById(R.id.mapLayout);
            mapShape.setRootLayout(rootLayout);
            mapShape.centerTo(item);

            FlorMap florMap = mMap.getFlorMap();
            String url = florMap.getImageUrl();
            ImageLoader.loadAsync(mapShape, url);

            TextView textView = findViewById(R.id.florNumber);
            textView.setText("" + florMap.getFlorNumber());
        }
    }

    protected void checkLeftArrow() {
        ImageButton button = (ImageButton) findViewById(R.id.buttonLeft);
        if (!activeLeftArrow()) {
            button.setVisibility(View.INVISIBLE);
        }
        button.setOnClickListener(new AdapterView.OnClickListener() {
            @Override
            public void onClick(View v) {
                actionLeftArrow();
            }
        });
    }

    protected boolean activeLeftArrow() {
        boolean result = false;
        if (item != null && item.containsAdditionalImages()) {
            result = true;
        }
        return result;
    }

    protected void actionLeftArrow() {
        if (item != null && item.containsAdditionalImages()) {
            String[] images = item.getAdditionalImagesUrl();

            imagePosition = (imagePosition + images.length) % (images.length + 1);

            ImageView imageView = findViewById(R.id.centralImage);
            String url;
            if (imagePosition == 0) {
                url = item.getImageUrl();
            } else {
                url = images[imagePosition - 1];
            }
            ImageLoader.loadAsync(imageView, imageView, url);
        }
    }

    protected void checkRightArrow() {
        ImageButton button = (ImageButton) findViewById(R.id.buttonRight);
        if (!activeRightArrow()) {
            button.setVisibility(View.INVISIBLE);
        }
        button.setOnClickListener(new AdapterView.OnClickListener() {
            @Override
            public void onClick(View v) {
                actionRightArrow();
            }
        });
    }

    protected boolean activeRightArrow() {
        boolean result = false;
        if (item != null && item.containsAdditionalImages()) {
            result = true;
        }
        return result;
    }

    protected void actionRightArrow() {
        if (item != null && item.containsAdditionalImages()) {
            String[] images = item.getAdditionalImagesUrl();
            imagePosition = (imagePosition + 1) % (images.length + 1);

            ImageView imageView = findViewById(R.id.centralImage);
            String url = null;
            if (imagePosition == 0) {
                url = item.getImageUrl();
            } else {
                url = images[imagePosition - 1];
            }
            ImageLoader.loadAsync(imageView, imageView, url);
        }
    }

    protected void checkTopArrow() {
        ImageButton button = (ImageButton) findViewById(R.id.buttonTop);
        if (!activeTopArrow()) {
            button.setVisibility(View.INVISIBLE);
        }
        button.setOnClickListener(new AdapterView.OnClickListener() {
            @Override
            public void onClick(View v) {
                actionTopArrow();
            }
        });

    }

    protected boolean activeTopArrow() {
        boolean result = false;
        if (mMap != null && mMap.containsContent()) {
            result = true;
        }
        return result;
    }

    protected void actionTopArrow() {
        if (!showTopExtra) {
            showTopExtra = true;
            showCenterExtra = false;
        } else {
            showTopExtra = false;
            showCenterExtra = false;
        }
        checkExtra();
    }

    protected void checkBottomArrow() {
        ImageButton button = (ImageButton) findViewById(R.id.buttonBottom);
        if (!activeBottomArrow()) {
            button.setVisibility(View.INVISIBLE);
        }
        button.setOnClickListener(new AdapterView.OnClickListener() {
            @Override
            public void onClick(View v) {
                actionBottomArrow();
            }
        });
    }

    protected boolean activeBottomArrow() {
        boolean result = isActiveDetails(item) || isActiveAudio(item) || isActiveVideo(item) || isActiveGallery(item);
        return result;
    }

    protected void actionBottomArrow() {
        if (!showBottomExtra) {
            showBottomExtra = true;
            showCenterExtra = false;
            showTopExtra = false;
        } else {
            showBottomExtra = false;
            showCenterExtra = false;
            showTopExtra = false;
        }
        checkExtra();
    }

    protected void checkExtra() {
        checkTopExtra();
        checkCenterExtra();
        checkBottomExtra();
    }

    @Override
    public void onStop() {
        super.onStop();
        MapShape mapShape = findViewById(R.id.mapImage);
        if (mapShape != null) {
            DataProvider.getLocationProvider().unregisterLocationChangeListener(mapShape);
        }
    }

    protected void updateSelectedItem(Item item) {
        updateItem(item);

        checkTop();
        checkCenter();
        checkBottom();

        checkExtra();
    }

    private void updateItem(Item item) {
        this.item = item;
        this.mMap = getMapItem(item);
    }
}
