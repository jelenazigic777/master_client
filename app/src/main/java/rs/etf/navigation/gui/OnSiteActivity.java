package rs.etf.navigation.gui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import rs.etf.navigation.R;
import rs.etf.navigation.db.DataProvider;
import rs.etf.navigation.gui.audio.AudioActivity;
import rs.etf.navigation.gui.augmented.LoadingScanActivity;
import rs.etf.navigation.gui.common.SimpleActivity;
import rs.etf.navigation.gui.maps.MapItemsActivity;
import rs.etf.navigation.gui.navigation.NavigationSelectActivity;
import rs.etf.navigation.place.Entry;
import rs.etf.navigation.place.Exhibition;
import rs.etf.navigation.place.Item;
import rs.etf.navigation.place.Museum;
import rs.etf.navigation.place.MuseumMap;

import static rs.etf.navigation.gui.Constants.EXHIBITIONS;
import static rs.etf.navigation.gui.Constants.MUSEUM_ITEMS_MAPS;

public class OnSiteActivity extends SimpleActivity {

    public static ImageLoader imageLoader = ImageLoader.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Museum museum = DataProvider.getMuseum();
        ImageView viewOnSite = findViewById(R.id.activity_on_site);
        if (museum.getSecondaryImage() != null) {
            OnSiteActivity.imageLoader.init(ImageLoaderConfiguration.createDefault(getApplicationContext()));
            imageLoader.displayImage(museum.getSecondaryImage(), viewOnSite);
        } else {
            viewOnSite.setImageResource(R.drawable.muzej_2);
        }
        delay();

    }

    @Override
    public int getActivityLayout() {
        return R.layout.activity_on_site;
    }

    private void delay() {
        createActions();
    }

    private void createActions() {

        View buttonAudioGuid = findViewById(R.id.buttonAudioGuid);
        setValues(buttonAudioGuid, getString(R.string.audio_giude), getString(R.string.walk_and_listem));
        buttonAudioGuid.setOnClickListener(new AdapterView.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OnSiteActivity.this, AudioActivity.class);
                startActivity(intent);
            }
        });

        View buttonItem = findViewById(R.id.buttonAugmented);
        setValues(buttonItem, getString(R.string.augmented_reality), getString(R.string.is_there_something_more));
        buttonItem.setOnClickListener(new AdapterView.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OnSiteActivity.this, LoadingScanActivity.class);
                startActivity(intent);
            }
        });

        View buttonMaps = findViewById(R.id.buttonMaps);
        setValues(buttonMaps, getString(R.string.maps), getString(R.string.locate_items_on_a_map));
        buttonMaps.setOnClickListener(new AdapterView.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OnSiteActivity.this, MapItemsActivity.class);
                List<MuseumMap> maps = DataProvider.getMuseumMaps();
                List<Entry<Item, MuseumMap>> itemsMaps = new LinkedList<>();
                for (MuseumMap museumMap : maps) {
                    itemsMaps.addAll(museumMap.getItemsMaps());
                }
                intent.putExtra(MUSEUM_ITEMS_MAPS, (Serializable) itemsMaps);
                startActivity(intent);
            }
        });

        View buttonNavigation = findViewById(R.id.buttonNavigation);
        setValues(buttonNavigation, getString(R.string.navigation), getString(R.string.take_me_there));
        buttonNavigation.setOnClickListener(new AdapterView.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OnSiteActivity.this, NavigationSelectActivity.class);
                List<Exhibition> items = DataProvider.getMuseum().getExhibitions();
                intent.putExtra(EXHIBITIONS, (Serializable) items);
                startActivity(intent);
            }
        });

    }


    private void setValues(View view, String title, String description) {
        TextView text = view.findViewById(R.id.textTitle);
        text.setText(title);

        TextView textDescription = view.findViewById(R.id.textDescription);
        textDescription.setText(description);
    }

}
