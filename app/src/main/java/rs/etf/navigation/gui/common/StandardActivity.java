package rs.etf.navigation.gui.common;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import rs.etf.navigation.R;
import rs.etf.navigation.place.Item;

import static rs.etf.navigation.gui.Constants.ITEM;

public abstract class StandardActivity extends AppCompatActivity {
    //TODO nije napisano
    protected Item item;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getActivityLayout());
        getSupportActionBar().hide();

        extractState(savedInstanceState);

        checkTop();
        checkCenter();
        checkBottom();

        checkExtra();

    }

    abstract int getActivityLayout();

    abstract Item getNextItem();

    protected static final String IMAGE_POSITION = "IMAGE_POSITION";
    protected static final String SHOW_TOP_EXTRA = "SHOW_TOP_EXTRA";
    protected static final String SHOW_CENTER_EXTRA = "SHOW_CENTER_EXTRA";
    protected static final String SHOW_BOTTOM_EXTRA = "SHOW_BOTTOM_EXTRA";
    protected int imagePosition;
    protected boolean showTopExtra, showCenterExtra, showBottomExtra;

    protected void extractState(Bundle savedInstanceState) {
        item = getNextItem();
        if (savedInstanceState != null) {
            imagePosition = savedInstanceState.getInt(IMAGE_POSITION);
            showTopExtra = savedInstanceState.getBoolean(SHOW_TOP_EXTRA);
            showCenterExtra = savedInstanceState.getBoolean(SHOW_CENTER_EXTRA);
            showBottomExtra = savedInstanceState.getBoolean(SHOW_BOTTOM_EXTRA);
        } else {
            imagePosition = 0;
            showTopExtra = false;
            showCenterExtra = false;
            showBottomExtra = false;
        }
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt(IMAGE_POSITION, imagePosition);
        outState.putBoolean(SHOW_TOP_EXTRA, showTopExtra);
        outState.putBoolean(SHOW_CENTER_EXTRA, showCenterExtra);
        outState.putBoolean(SHOW_BOTTOM_EXTRA, showBottomExtra);
    }

    protected void checkTop() {

    }

    protected void checkCenter() {
        if (item != null) {
            checkCenterImage();

            checkLeftArrow();
            checkRightArrow();
            checkTopArrow();
            checkBottomArrow();
        }

    }

    abstract void checkCenterImage();

    abstract void checkBottom();

    protected void checkBottomExtra() {
        LinearLayout layout = (LinearLayout) findViewById(R.id.additionalBottomLayout);
        if (showBottomExtra) {
            checkDetails(layout);
            checkAudio(layout);
            checkVideo(layout);
            checkGallery(layout);
        } else {
            layout.removeAllViews();
        }
    }

    protected void checkDetails(LinearLayout layout) {
        if (activeDetails()) {
            View view = createDetailElementView(layout, getString(R.string.details), R.drawable.ic_photo_black_24dp);
            view.setOnClickListener(new AdapterView.OnClickListener() {
                @Override
                public void onClick(View v) {
                    actionDetails();
                }
            });
        }
    }

    @NonNull
    protected View createDetailElementView(LinearLayout layout, String title, int icon) {
        LayoutInflater inflater = (LayoutInflater) layout.getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.layout_additiona_bottom_element, layout, false);

        TextView textView = (TextView) view.findViewById(R.id.title);
        textView.setText(title);

        ImageView imageView = (ImageView) view.findViewById(R.id.icon);
        imageView.setImageResource(icon);

        layout.addView(view);
        return view;
    }

    protected void actionDetails() {
        Intent intent = new Intent(this, DescriptionActivity.class);
        intent.putExtra(ITEM, item);
        startActivity(intent);
    }

    protected boolean activeDetails() {
        boolean result = false;
        if (item != null && item.getTextUrl() != null) {
            result = true;
        }
        return result;
    }

    protected void checkAudio(LinearLayout layout) {
        if (activeAudio()) {
            View view = createDetailElementView(layout, "Audio", R.drawable.ic_volume_down_black_24dp);
            view.setOnClickListener(new AdapterView.OnClickListener() {
                @Override
                public void onClick(View v) {
                    actionAudio();
                }
            });
        }
    }

    protected void actionAudio() {
        Intent intent = new Intent(this, AudioSingleActivity.class);
        intent.putExtra(ITEM, item);
        startActivity(intent);
    }

    protected boolean activeAudio() {
        boolean result = false;
        if (item != null && item.containsAudio()) {
            result = true;
        }
        return result;
    }

    protected void checkVideo(LinearLayout layout) {
        if (activeVideo()) {
            View view = createDetailElementView(layout, "Video", R.drawable.ic_videocam_black_24dp);
            view.setOnClickListener(new AdapterView.OnClickListener() {
                @Override
                public void onClick(View v) {
                    actionVideo();
                }
            });
        }
    }

    protected void actionVideo() {
        VideoPlayerStarter.play(this, item);
    }

    protected boolean activeVideo() {
        boolean result = false;
        if (item != null && item.containsVideo()) {
            result = true;
        }
        return result;
    }

    protected void checkGallery(LinearLayout layout) {
        if (activeGallery()) {
            View view = createDetailElementView(layout, "Gallery", R.drawable.ic_photo_album_black_24dp);
            view.setOnClickListener(new AdapterView.OnClickListener() {
                @Override
                public void onClick(View v) {
                    actionGallery();
                }
            });
        }
    }

    protected void actionGallery() {
        Intent intent = new Intent(this, GalleryActivity.class);
        intent.putExtra(ITEM, item);
        startActivity(intent);
    }

    protected boolean activeGallery() {
        boolean result = false;
        if (!(item == null || item.getAdditionalImagesUrl() == null || item.getAdditionalImagesUrl().length == 0)) {
            result = true;
        }
        return result;
    }

    protected void checkCenterExtra() {
        if (showCenterExtra) {

        } else {

        }
    }

    protected void checkTopExtra() {
        if (showTopExtra) {

        } else {

        }
    }

    protected void checkLeftArrow() {
        ImageButton button = (ImageButton) findViewById(R.id.buttonLeft);
        if (!activeLeftArrow()) {
            button.setVisibility(View.INVISIBLE);
        }
        button.setOnClickListener(new AdapterView.OnClickListener() {
            @Override
            public void onClick(View v) {
                actionLeftArrow();
            }
        });
    }

    protected boolean activeLeftArrow() {
        boolean result = false;

        return result;
    }

    protected void actionLeftArrow() {
        if (item == null) return;
        String[] images = item.getAdditionalImagesUrl();
        if (images != null && images.length != 0) {
            imagePosition = (imagePosition + 1) % (images.length + 1);

            ImageView imageView = findViewById(R.id.centralImage);
            String url = null;
            if (imagePosition == 0) {
                url = item.getImageUrl();
            } else {
                url = images[imagePosition - 1];

            }
            ImageLoader.loadAsync(imageView, imageView, url);
        }
    }

    protected void checkRightArrow() {
        ImageButton button = (ImageButton) findViewById(R.id.buttonRight);
        if (!activeRightArrow()) {
            button.setVisibility(View.INVISIBLE);
        }
        button.setOnClickListener(new AdapterView.OnClickListener() {
            @Override
            public void onClick(View v) {
                actionRightArrow();
            }
        });
    }

    protected boolean activeRightArrow() {
        boolean result = false;

        return result;
    }

    protected void actionRightArrow() {
        if (item == null) return;
        String[] images = item.getAdditionalImagesUrl();
        if (images != null && images.length != 0) {
            imagePosition = (imagePosition + images.length) % (images.length + 1);

            ImageView imageView = findViewById(R.id.centralImage);
            String url = null;
            if (imagePosition == 0) {
                url = item.getImageUrl();
            } else {
                url = images[imagePosition - 1];
            }
            ImageLoader.loadAsync(imageView, imageView, url);
        }
    }

    protected void checkTopArrow() {
        ImageButton button = (ImageButton) findViewById(R.id.buttonTop);
        if (!activeTopArrow()) {
            button.setVisibility(View.INVISIBLE);
        }
        button.setOnClickListener(new AdapterView.OnClickListener() {
            @Override
            public void onClick(View v) {
                actionTopArrow();
            }
        });

    }

    protected boolean activeTopArrow() {
        boolean result = false;

        return result;
    }

    protected void actionTopArrow() {
        if (!showBottomExtra) {
            showBottomExtra = true;
            showCenterExtra = false;
            showTopExtra = false;
        } else {
            showBottomExtra = false;
            showCenterExtra = false;
            showTopExtra = false;
        }
        checkExtra();
    }

    protected void checkBottomArrow() {
        ImageButton button = (ImageButton) findViewById(R.id.buttonBottom);
        if (!activeBottomArrow()) {
            button.setVisibility(View.INVISIBLE);
        }
        button.setOnClickListener(new AdapterView.OnClickListener() {
            @Override
            public void onClick(View v) {
                actionBottomArrow();
            }
        });
    }

    protected boolean activeBottomArrow() {
        boolean result = false;

        return result;
    }

    protected void actionBottomArrow() {
        if (!showBottomExtra) {
            showBottomExtra = true;
            showCenterExtra = false;
            showTopExtra = false;
        } else {
            showBottomExtra = false;
            showCenterExtra = false;
            showTopExtra = false;
        }

        checkExtra();
    }

    private void checkExtra() {
        checkTopExtra();
        checkCenterExtra();
        checkBottomExtra();
    }

}
