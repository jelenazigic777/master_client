package rs.etf.navigation.gui.common;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.concurrent.TimeUnit;

import rs.etf.navigation.R;

public class AudioProgress {
    private ProgressBar progressBarCircle;
    private CountDownTimer countDownTimer;
    private TextView remainingTime;
    private AudioProgressInterface mediaSource;

    public AudioProgress(Activity activity, AudioProgressInterface mediaSource) {
        progressBarCircle = (ProgressBar) activity.findViewById(R.id.progressBarCircle);
        remainingTime = (TextView) activity.findViewById(R.id.remainingTime);
        this.mediaSource = mediaSource;
    }

    public void startCountDownTimer() {
        prepareProgress();
        countDownTimer = new CountDownTimer(1000 * 60 * 60, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                try {
                    int progress = mediaSource.getMediaPlayer().getCurrentPosition();
                    progressBarCircle.setProgress(progress / 1000);
                    updateRemainingTime(progress, mediaSource.getDuration());
                } catch (Exception e) {
                    setProgressBarValues();
                }

            }

            @Override
            public void onFinish() {
                setProgressBarValues();
            }

        }.start();
        countDownTimer.start();
    }

    private void prepareProgress() {
        progressBarCircle.setVisibility(View.VISIBLE);
        progressBarCircle.setMax(mediaSource.getDuration() / 1000);
        remainingTime.setVisibility(View.VISIBLE);
    }

    public void hideProgress() {
        progressBarCircle.setVisibility(View.INVISIBLE);
        remainingTime.setVisibility(View.INVISIBLE);
    }

    public void stopCountDownTimer() {
        countDownTimer.cancel();
    }

    private void setProgressBarValues() {
        int duration = mediaSource.getDuration();
        progressBarCircle.setMax(duration / 1000);
        progressBarCircle.setProgress(duration / 1000);
        updateRemainingTime(duration, duration);
    }

    private void updateRemainingTime(int progress, int duration) {
        int milliSeconds = duration - progress;
        String hms;
        if (duration <= 60 * 60 * 1000) {
            hms = String.format("-%02d:%02d",
                    TimeUnit.MILLISECONDS.toMinutes(milliSeconds),
                    TimeUnit.MILLISECONDS.toSeconds(milliSeconds) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(milliSeconds)));
        } else {
            hms = String.format("-%02d:%02d:%02d",
                    TimeUnit.MILLISECONDS.toHours(milliSeconds),
                    TimeUnit.MILLISECONDS.toMinutes(milliSeconds) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(milliSeconds)),
                    TimeUnit.MILLISECONDS.toSeconds(milliSeconds) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(milliSeconds)));
        }
        remainingTime.setText(hms);

    }

    public interface AudioProgressInterface {
        int getDuration();

        MediaPlayer getMediaPlayer();
    }
}
