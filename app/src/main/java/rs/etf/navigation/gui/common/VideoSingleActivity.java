package rs.etf.navigation.gui.common;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.webkit.URLUtil;
import android.widget.MediaController;
import android.widget.VideoView;

import rs.etf.navigation.R;
import rs.etf.navigation.place.Item;

import static rs.etf.navigation.gui.Constants.ITEM;

public class VideoSingleActivity extends SimpleActivity {

    Item item;

    private VideoView mVideoView;

    public static final String VIDEO_STATE = "VIDEO_STATE";
    private int videoState = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle extras = getIntent().getExtras();
        item = (Item) extras.getSerializable(ITEM);

        extractState(savedInstanceState);

        mVideoView = findViewById(R.id.videoView);

        // Set up the media controller widget and attach it to the video view.
        MediaController controller = new MediaController(this);
        controller.setMediaPlayer(mVideoView);
        mVideoView.setMediaController(controller);
    }

    @Override
    public int getActivityLayout() {
        return R.layout.activity_video_single;
    }

    protected void extractState(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            videoState = savedInstanceState.getInt(VIDEO_STATE);
        } else {
            videoState = 0;
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(VIDEO_STATE, mVideoView.getCurrentPosition());
    }

    @Override
    protected void onStart() {
        super.onStart();

        // Load the media each time onStart() is called.
        initializePlayer();
    }

    @Override
    protected void onPause() {
        super.onPause();

        // In Android versions less than N (7.0, API 24), onPause() is the
        // end of the visual lifecycle of the app.  Pausing the video here
        // prevents the sound from continuing to play even after the app
        // disappears.
        //
        // This is not a problem for more recent versions of Android because
        // onStop() is now the end of the visual lifecycle, and that is where
        // most of the app teardown should take place.
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            mVideoView.pause();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        // Media playback takes a lot of resources, so everything should be
        // stopped and released at this time.
        releasePlayer();
    }


    private void initializePlayer() {
        if (item == null || !item.containsVideo() || !URLUtil.isValidUrl(item.getVideoUrl())) {
            return;
        }

        Uri videoUri = Uri.parse(item.getVideoUrl());
        mVideoView.setVideoURI(videoUri);

        // Listener for onPrepared() event (runs after the media is prepared).
        mVideoView.setOnPreparedListener(
                new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mediaPlayer) {

                        // Restore saved position, if available.
                        if (videoState > 0) {
                            mVideoView.seekTo(videoState);
                        } else {
                            // Skipping to 1 shows the first frame of the video.
                            mVideoView.seekTo(1);
                        }

                        // Start playing!
                        mVideoView.start();
                    }
                });

        // Listener for onCompletion() event (runs after media has finished
        // playing).
        mVideoView.setOnCompletionListener(
                new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mediaPlayer) {
                        mVideoView.seekTo(0);
                    }
                });
    }


    // Release all media-related resources. In a more complicated app this
    // might involve unregistering listeners or releasing audio focus.
    private void releasePlayer() {
        mVideoView.stopPlayback();
    }

}