package rs.etf.navigation.gui;

public class Constants {
    public static final String ITEM = "SharedItem";
    public static final String VISITED_ITEMS = "VisitedItems";
    public static final String EXHIBITIONS = "Exhibitions";
    public static final String ITEM_TO_PLAY = "ITEM_TO_PLAY";
    public static final String MAP_ITEM = "MAP_ITEM";
    public static final String MUSEUM_ITEMS_MAPS = "MUSEUM_ITEMS_MAPS";

    public static final int INTERNET_PERMISSIONS_REQUEST = 32;
    public static final int CAMERA_PERMISSIONS_REQUEST = 33;
    public static final int GPS_PERMISSIONS_REQUEST = 34;
    public static final int BLUETOOTH_PERMISSIONS_REQUEST = 35;
    public static final int WRITE_EXTERNAL_STORAGE_PERMISSIONS_REQUEST = 36;

    public static final String LOCATION_INTENT = "rs.etf.navigation.gui.LOCATION";
    public static final String LOCATION = "LOCATION";

}
