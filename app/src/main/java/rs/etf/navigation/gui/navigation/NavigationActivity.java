package rs.etf.navigation.gui.navigation;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import java.io.Serializable;
import java.util.List;

import rs.etf.navigation.R;
import rs.etf.navigation.db.DataProvider;
import rs.etf.navigation.gui.Constants;
import rs.etf.navigation.gui.common.ImageLoader;
import rs.etf.navigation.gui.common.ItemActivity;
import rs.etf.navigation.gui.exhibitions.ExhibitionActivity;
import rs.etf.navigation.gui.maps.MapItemsActivity;
import rs.etf.navigation.location.algorithms.dijkstra.PathFinder;
import rs.etf.navigation.place.Entry;
import rs.etf.navigation.place.Exhibition;
import rs.etf.navigation.place.Item;
import rs.etf.navigation.place.MuseumMap;

public class NavigationActivity extends ExhibitionActivity {

    @Override
    protected View.OnLongClickListener getOnLongClickListener() {
        return new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (item.containsContent()) {
                    Exhibition exhibition = (Exhibition) item;
                    List<Item> items = exhibition.getItems();
                    Intent intent = new Intent(NavigationActivity.this, MapItemsActivity.class);
                    Item tempItem = items.get(imagePosition);
                    List<Entry<Item, MuseumMap>> maps = PathFinder.findPath(DataProvider.getMuseum(), null, tempItem);
                    //TODO negde u aplikaciji treba ostaviti mogucnost da se putanja trazi ne od ulaza nego od trenutne lokacije.
                    intent.putExtra(Constants.MUSEUM_ITEMS_MAPS, (Serializable) maps);
                    startActivity(intent);
                }
                return false;
            }
        };
    }
}

