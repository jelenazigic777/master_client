package rs.etf.navigation.gui.maps;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import java.util.HashMap;
import java.util.Map;

import rs.etf.navigation.R;
import rs.etf.navigation.location.Location;
import rs.etf.navigation.location.LocationChangeListener;
import rs.etf.navigation.place.CurrentLocation;
import rs.etf.navigation.place.Item;
import rs.etf.navigation.place.MuseumMap;

public class MapShape extends AppCompatImageView implements LocationChangeListener {

    MuseumMap map;
    CurrentLocation currentLocation;

    private Paint itemPaint, locationPaint, announcementPaint;

    public MapShape(Context context) {
        super(context);
        init((AttributeSet) null);
    }

    public MapShape(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public MapShape(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    private void init(@Nullable AttributeSet set) {
        itemPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        itemPaint.setColor(getResources().getColor(R.color.itemColor));
        locationPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        locationPaint.setColor(getResources().getColor(R.color.locationColor));
        announcementPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        announcementPaint.setColor(getResources().getColor(R.color.announcementColor));
        position = null;
        currentLocation = new CurrentLocation();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        calcScale();
        if (position != null) {
            drawLocation(canvas);
            drawItems(canvas);
            drawSelected(canvas);
        }
    }

    private void drawItems(Canvas canvas) {
        if (map.getItems() != null) {
            for (Item item : map.getItems()) {
                if (item != selectedItem) {
                    PointF point = getPoint(item);
                    drawItem(canvas, point.x, point.y, item);
                }
            }
        }
    }

    int[] position;
    float distance;
    float scale;
    Map<Item, PointF> points;

    protected PointF calcPoint(Location location) {
        PointF result = new PointF();
        //TODO potrebno je uraditi transformaciju iz geografske sirine i duzine u x y koordinate
        float x = position[0] + (float) (scale * location.getX());
        float y = position[1] + (float) (scale * location.getY());
        result.x = x;
        result.y = y;
        return result;
    }

    @NonNull
    private PointF getPoint(Item item) {
        PointF point = points.get(item);
        if (point == null) {
            point = calcPoint(item.getLocation());
            points.put(item, point);
        }
        return point;
    }

    private int[] calcScale() {
        if (position == null && map != null && map.containsContent()) {
            //racuna samo jednom
            position = ImageViewManipulator.getBitmapPositionInsideImageView(this);
            if (position != null) {
                Location start = map.getFlorMap().getStart();
                Location end = map.getFlorMap().getEnd();
                distance = (float) (end.getY() - start.getY());
                scale = position[3] / distance;
                points = new HashMap<>();
                windowWidth = rootLayout.getWidth();
                windowHeight = rootLayout.getHeight();
            }
        }
        return position;
    }

    protected void drawItem(Canvas canvas, float x, float y, Item item) {
        switch (item.getType()) {
            case Item.ANNOUNCEMENT:
                break;
            case Item.STAIRCASE_UP:
                break;
            case Item.STAIRCASE_DOWN:
                break;
            case Item.CURRENT_LOCATION:
                drawLocation(canvas, x, y, item);
                break;
            default:
                drawItem(canvas, x, y);
                break;
        }
    }

    private void drawItem(Canvas canvas, float x, float y) {
        canvas.drawRect(x, y, x + 25, y + 25, itemPaint);
    }

    private void drawSelectedItem(Canvas canvas, float x, float y) {
        canvas.drawRect(x - 8, y - 8, x + 33, y + 33, locationPaint);
        canvas.drawRect(x, y, x + 25, y + 25, itemPaint);
    }

    private void drawLocation(Canvas canvas, float x, float y, Item item) {
        Location location = item.getLocation();

        double weight = location.getWeight();
        int radius = (int) (weight * scale);
        canvas.drawCircle(x, y, radius, locationPaint);
        canvas.drawCircle(x, y, 10, itemPaint);
    }

    private void drawLocation(Canvas canvas) {
        if (currentLocation.containsLocation()) {
            PointF point = calcPoint(currentLocation.getLocation());
            drawItem(canvas, point.x, point.y, currentLocation);
        }
    }


    ViewGroup rootLayout;
    int windowWidth;
    int windowHeight;

    public int getWindowWidth() {
        return windowWidth;
    }

    public int getWindowHeight() {
        return windowHeight;
    }

    public void setRootLayout(View rootLayout) {
        this.rootLayout = (ViewGroup) rootLayout;
        //TODO ovo je proba za TouchImageView
        this.setOnTouchListener(new MapShapeTouchListener(this));
    }

    protected Item selectedItem;

    public Item getSelectedItem() {
        return selectedItem;
    }

    public void setSelectedItem(Item selectedItem) {
        this.selectedItem = selectedItem;
    }

    public void centerTo(Item item) {
        setSelectedItem(item);
        if (position != null && item.containsLocation()) {
            //TODO ovo ne centrira jer kada se postavi nova mapa position se postavlja na null! Na null vrednost se postavio jer je iyracunavanje koordinata u nekim slucajevima davalo infinity vrednost

            PointF point = calcPoint(item.getLocation());

            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) this.getLayoutParams();
            lp.leftMargin = -(int) (point.x - windowWidth / 2);
            lp.topMargin = -(int) (point.y - windowHeight / 2);
            lp.rightMargin = this.getWidth() - lp.leftMargin - windowWidth;
            lp.bottomMargin = this.getHeight() - lp.topMargin - windowHeight;
            this.setLayoutParams(lp);
        }
    }

    private void drawSelected(Canvas canvas) {
        if (selectedItem != null && position != null && selectedItem.containsLocation()) {
            PointF point = calcPoint(selectedItem.getLocation());
            drawSelectedItem(canvas, point.x, point.y);
        }
    }

    public void center() {
        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) this.getLayoutParams();
        lp.leftMargin = 0;
        lp.topMargin = 0;
        lp.rightMargin = 0;
        lp.bottomMargin = 0;
        this.setLayoutParams(lp);
    }

    public void setMap(MuseumMap map) {
        this.map = map;
        position = null;
    }

    public MuseumMap getMap() {
        return this.map;
    }

    @Override
    public void onLocationChange(Location location) {
        this.currentLocation.setLocation(location);
        this.invalidate();
    }
}
