package rs.etf.navigation.gui.exhibitions;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import java.io.Serializable;
import java.util.List;

import rs.etf.navigation.R;
import rs.etf.navigation.gui.Constants;
import rs.etf.navigation.gui.common.ImageLoader;
import rs.etf.navigation.gui.common.ImageTouchListener;
import rs.etf.navigation.gui.common.ItemActivity;
import rs.etf.navigation.gui.maps.MapItemsActivity;
import rs.etf.navigation.place.Entry;
import rs.etf.navigation.place.Exhibition;
import rs.etf.navigation.place.Item;
import rs.etf.navigation.place.MuseumMap;

public class ExhibitionActivity extends ItemActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public int getActivityLayout() {
        return R.layout.activity_exhibition;
    }

    @Override
    protected void checkCenterImage() {
        String url = item.getImageUrl();
        ImageView imageView = findViewById(R.id.centralImage);
        imageView.setOnTouchListener(new ImageTouchListener(this));
        ImageLoader.loadAsync(this, imageView, url);
        imageView.setOnLongClickListener(getOnLongClickListener());
    }

    protected View.OnLongClickListener getOnLongClickListener() {
        return new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                gotoMaps();
                return false;
            }
        };
    }

    protected void gotoMaps() {
        Intent intent = new Intent(ExhibitionActivity.this, MapItemsActivity.class);
        Exhibition exhibition = (Exhibition) item;
        List<Entry<Item, MuseumMap>> maps = exhibition.getItemsMaps();
        intent.putExtra(Constants.MUSEUM_ITEMS_MAPS, (Serializable) maps);
        startActivity(intent);
    }

    protected void gotoItem() {
        Intent intent = new Intent(ExhibitionActivity.this, ItemActivity.class);
        Exhibition exhibition = (Exhibition) item;
        List<Item> items = exhibition.getItems();
        Item tempItem = items.get(imagePosition);
        intent.putExtra(Constants.ITEM, tempItem);
        startActivity(intent);
    }

    @Override
    protected void checkTopArrow() {
    }

    @Override
    protected void checkBottomArrow() {
    }

    @Override
    protected void checkTopExtra() {
    }

    @Override
    protected void checkBottomExtra() {
    }

    protected boolean activeLeftArrow() {
        boolean result = false;
        Exhibition exhibition = (Exhibition) item;
        if (!(exhibition == null || exhibition.getItems() == null || exhibition.getItems().size() <= 1)) {
            result = true;
        }
        return result;
    }

    protected void actionLeftArrow() {
        if (item != null && item.containsContent()) {
            Exhibition exhibition = (Exhibition) item;
            List<Item> items = exhibition.getItems();

            imagePosition = (imagePosition + items.size()) % (items.size() + 1);

            ImageView imageView = findViewById(R.id.centralImage);
            String url;
            if (imagePosition == 0) {
                url = item.getImageUrl();
            } else {
                url = items.get(imagePosition - 1).getImageUrl();
            }
            ImageLoader.loadAsync(imageView, imageView, url);
        }
    }


    protected boolean activeRightArrow() {
        boolean result = false;
        Exhibition exhibition = (Exhibition) item;
        if (!(exhibition == null || exhibition.getItems() == null || exhibition.getItems().size() <= 1)) {
            result = true;
        }
        return result;
    }

    protected void actionRightArrow() {
        if (item != null && item.containsContent()) {
            Exhibition exhibition = (Exhibition) item;
            List<Item> items = exhibition.getItems();

            imagePosition = (imagePosition + 1) % (items.size() + 1);

            ImageView imageView = findViewById(R.id.centralImage);
            String url;
            if (imagePosition == 0) {
                url = item.getImageUrl();
            } else {
                url = items.get(imagePosition - 1).getImageUrl();
            }
            ImageLoader.loadAsync(imageView, imageView, url);
        }
    }

}

