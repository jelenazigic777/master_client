package rs.etf.navigation.gui;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.transition.Scene;
import android.transition.Slide;
import android.transition.Transition;
import android.transition.TransitionManager;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.io.Serializable;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import rs.etf.navigation.db.DataProvider;
import rs.etf.navigation.gui.common.VisitedItemsActivity;
import rs.etf.navigation.gui.exhibitions.ExhibitionsActivity;
import rs.etf.navigation.gui.common.SimpleActivity;
import rs.etf.navigation.place.Exhibition;
import rs.etf.navigation.location.LocationProvider;
import rs.etf.navigation.place.Item;

import static rs.etf.navigation.gui.Constants.BLUETOOTH_PERMISSIONS_REQUEST;
import static rs.etf.navigation.gui.Constants.CAMERA_PERMISSIONS_REQUEST;
import static rs.etf.navigation.gui.Constants.WRITE_EXTERNAL_STORAGE_PERMISSIONS_REQUEST;
import static rs.etf.navigation.gui.Constants.EXHIBITIONS;
import static rs.etf.navigation.gui.Constants.GPS_PERMISSIONS_REQUEST;
import static rs.etf.navigation.gui.Constants.INTERNET_PERMISSIONS_REQUEST;
import static rs.etf.navigation.gui.Constants.VISITED_ITEMS;

import rs.etf.navigation.R;
import rs.etf.navigation.place.Museum;

public class TestActivity extends SimpleActivity {

    public static Context context;

    public static ImageLoader imageLoader = ImageLoader.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Museum museum = DataProvider.getMuseum();
        ImageView viewTest = findViewById(R.id.activity_test);
        if (museum.getPrimaryImage() != null) {
            TestActivity.imageLoader.init(ImageLoaderConfiguration.createDefault(getApplicationContext()));
            imageLoader.displayImage(museum.getPrimaryImage(), viewTest);
        } else {
            viewTest.setImageResource(R.drawable.muzej_1);
        }
        delay();
        context = this; //TODO ovo nije ispravno
    }

    @Override
    public int getActivityLayout() {
        return R.layout.activity_test;
    }

    private void delay() {
        final View view = findViewById(R.id.scene_root);
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                view.post(new Runnable() {
                    @Override
                    public void run() {
                        createScene();
                        createActions();
                        getPermissions();

                    }
                });
            }
        }, 3 * 1000);
    }

    private void createActions() {

        View buttonOnSite = findViewById(R.id.onSite);
        setValues(buttonOnSite, getString(R.string.take_a_walk), getString(R.string.explore_museum));
        buttonOnSite.setOnClickListener(new AdapterView.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TestActivity.this, OnSiteActivity.class);
                startActivity(intent);
            }
        });

        View buttonExhibitions = findViewById(R.id.buttonExhibitions);
        setValues(buttonExhibitions, getString(R.string.view_exhibition), getString(R.string.what_we_have_prepared));
        buttonExhibitions.setOnClickListener(new AdapterView.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TestActivity.this, ExhibitionsActivity.class);
                List<Exhibition> items = DataProvider.getMuseum().getExhibitions();
                intent.putExtra(EXHIBITIONS, (Serializable) items);
                startActivity(intent);
            }
        });


        View buttonHistory = findViewById(R.id.buttonHistory);
        setValues(buttonHistory, getString(R.string.visited_items), getString(R.string.thinks_you_have_visited));
        buttonHistory.setOnClickListener(new AdapterView.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TestActivity.this, VisitedItemsActivity.class);
                List<Item> items = DataProvider.getAllItems();//TODO staviti da se popunjava DataProvider.getVisitedItems();
                intent.putExtra(VISITED_ITEMS, (Serializable) items);
                startActivity(intent);
            }
        });

    }


    private void setValues(View view, String title, String description) {
        TextView text = view.findViewById(R.id.textTitle);
        text.setText(title);

        TextView textDescription = view.findViewById(R.id.textDescription);
        textDescription.setText(description);
    }

    private void getPermissions() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.INTERNET}, INTERNET_PERMISSIONS_REQUEST);
        }
        if (ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CAMERA}, CAMERA_PERMISSIONS_REQUEST);
        }

        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, CAMERA_PERMISSIONS_REQUEST);
        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION}, GPS_PERMISSIONS_REQUEST);
        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH_ADMIN) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.BLUETOOTH, android.Manifest.permission.BLUETOOTH_ADMIN}, BLUETOOTH_PERMISSIONS_REQUEST);
        }
    }

    boolean enabledInternet = false, enabledCamera = false, enabledBluetooth = false, enabledGPS = false, writeExteralStorage = false;

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case INTERNET_PERMISSIONS_REQUEST: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    enabledInternet = true;
                } else {
                    Toast toast = Toast.makeText(TestActivity.this, getString(R.string.permission_denied_internet), Toast.LENGTH_LONG);
                    toast.show();
                }
                break;
            }
            case CAMERA_PERMISSIONS_REQUEST: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    enabledCamera = true;
                } else {
                    Toast toast = Toast.makeText(TestActivity.this, getString(R.string.permission_denied_camera), Toast.LENGTH_LONG);
                    toast.show();
                }
                break;
            }
            case WRITE_EXTERNAL_STORAGE_PERMISSIONS_REQUEST: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    writeExteralStorage = true;
                } else {
                    Toast toast = Toast.makeText(TestActivity.this, getString(R.string.permission_denied_write_external_storage), Toast.LENGTH_LONG);
                    toast.show();
                }
                break;
            }
            case GPS_PERMISSIONS_REQUEST: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    enabledGPS = true;
                } else {
                    Toast toast = Toast.makeText(TestActivity.this, getString(R.string.permission_denied_gps), Toast.LENGTH_LONG);
                    toast.show();
                }
                break;
            }
            case BLUETOOTH_PERMISSIONS_REQUEST: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    enabledGPS = true;
                } else {
                    Toast toast = Toast.makeText(TestActivity.this, getString(R.string.permission_denied_bluetooth), Toast.LENGTH_LONG);
                    toast.show();
                }
                break;
            }
        }
    }

    private void createScene() {
        ViewGroup mSceneRoot = (ViewGroup) findViewById(R.id.scene_root);

        Scene welcomeScene = Scene.getSceneForLayout(mSceneRoot, R.layout.scene_welcome, this);
        Scene startScene = Scene.getSceneForLayout(mSceneRoot, R.layout.scene_start, this);
        Transition transition = new Slide(Gravity.RIGHT);
        //transition = TransitionInflater.from(this).inflateTransition(R.transition.fade_transition);
        transition.setDuration(1000);
        TransitionManager.go(startScene, transition);
    }

    boolean started = false;

    @Override
    public void onStart() {
        super.onStart();

        if (!started) {
            //TODO ovo sme da se uradi samo jednom inace ostaje neko ko nije rgistrtovan
            LocationProvider provider = DataProvider.getLocationProvider();
            provider.init(this);
            new TestLocationThread(this.getApplicationContext()).start();
            started = true;
        }

    }

    @Override
    public void onStop() {
        super.onStop();
    }

}
