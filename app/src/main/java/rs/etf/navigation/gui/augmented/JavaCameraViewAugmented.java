package rs.etf.navigation.gui.augmented;

import android.content.Context;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.util.AttributeSet;

import com.google.android.gms.vision.CameraSource;

import org.opencv.android.JavaCameraView;

public class JavaCameraViewAugmented extends JavaCameraView {

    public JavaCameraViewAugmented(Context context, int cameraId) {
        super(context, cameraId);
    }

    public JavaCameraViewAugmented(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCameraIndex();
    }

    private void setCameraIndex() {
        int position = -1;
        int id = -1;
        CameraManager manager = (CameraManager) getContext().getSystemService(Context.CAMERA_SERVICE);
        try {
            for (String cameraId : manager.getCameraIdList()) {
                id++;
                CameraCharacteristics chars = manager.getCameraCharacteristics(cameraId);
                Integer facing = chars.get(CameraCharacteristics.LENS_FACING);
                boolean f = facing == CameraMetadata.LENS_FACING_BACK;
                if (f) {
                    position = id;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (position >= 0) {
            this.setCameraIndex(position);
        } else if (id >= 0) {
            this.setCameraIndex(id);
        }
    }

    public void check(int width, int height) {
        float temScale = mScale;

        if (width != mFrameWidth || height != mFrameHeight) {
            int tempWidth = mFrameWidth;
            int tempHeight = mFrameHeight;
            mFrameWidth = width;
            mFrameHeight = height;
            AllocateCache();
            mFrameWidth = tempWidth;
            mFrameHeight = tempHeight;
            olsScale = mScale;
            mScale = 0;
        }

    }

    float olsScale = -1;

    public void rescale() {
        if (olsScale < 0) {
            olsScale = mScale;
            mScale = 0;
        }

    }
}
