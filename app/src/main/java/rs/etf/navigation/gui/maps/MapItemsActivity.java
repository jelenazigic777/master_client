package rs.etf.navigation.gui.maps;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import rs.etf.navigation.R;
import rs.etf.navigation.db.DataProvider;
import rs.etf.navigation.gui.common.ImageLoader;
import rs.etf.navigation.gui.common.ItemActivity;
import rs.etf.navigation.gui.common.SimpleActivity;
import rs.etf.navigation.place.Entry;
import rs.etf.navigation.place.FlorMap;
import rs.etf.navigation.place.Item;
import rs.etf.navigation.place.MuseumMap;

import static rs.etf.navigation.gui.Constants.ITEM;
import static rs.etf.navigation.gui.Constants.MUSEUM_ITEMS_MAPS;

public class MapItemsActivity extends SimpleActivity {

    protected List<Entry<Item, MuseumMap>> items;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle extras = getIntent().getExtras();
        items = (List<Entry<Item, MuseumMap>>) extras.getSerializable(MUSEUM_ITEMS_MAPS);

        LinearLayout listView = findViewById(R.id.mapItems);
        MapItemsAdapter adapter = new MapItemsAdapter(this, items);

        int i = 0;
        for (Entry<Item, MuseumMap> item : items) {
            View view = adapter.getView(i, null, listView);
            view.setId(i++);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int id = v.getId();
                    checkCenterImage(id);
                }

            });
            if (item.getKey().containsIAVContent()) {
                view.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        int id = v.getId();
                        itemsActivity(id);
                        return false;
                    }
                });
            }

            listView.addView(view);
        }
        checkCenterImage(0);

    }

    private void itemsActivity(int position) {
        if (items == null || items.size() < position || items.get(position) == null) return;
        Entry<Item, MuseumMap> item = items.get(position);
        Intent intent = new Intent(MapItemsActivity.this, ItemActivity.class);
        intent.putExtra(ITEM, item.getKey());
        startActivity(intent);
    }

    @Override
    public int getActivityLayout() {
        return R.layout.activity_map_items;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    private void checkCenterImage(int position) {
        if (items == null || items.size() < position || items.get(position) == null) return;
        Entry<Item, MuseumMap> item = items.get(position);
        MuseumMap mMap = item.getValue();
        if (mMap != null) {
            MapShape mapShape = findViewById(R.id.mapImage);
            DataProvider.getLocationProvider().registerLocationChangeListener(mapShape);
            mapShape.setMap(mMap);

            ViewGroup rootLayout = findViewById(R.id.mapLayout);
            mapShape.setRootLayout(rootLayout);
            mapShape.centerTo(item.getKey());

            FlorMap florMap = mMap.getFlorMap();
            String url = florMap.getImageUrl();
            ImageLoader.loadAsync(mapShape, url);

            TextView textView = findViewById(R.id.florNumber);
            textView.setText("" + florMap.getFlorNumber());
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        MapShape mapShape = findViewById(R.id.mapImage);
        if (mapShape != null) {
            DataProvider.getLocationProvider().unregisterLocationChangeListener(mapShape);
        }
    }

}
