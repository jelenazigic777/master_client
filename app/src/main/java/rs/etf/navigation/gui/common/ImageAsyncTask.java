package rs.etf.navigation.gui.common;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import java.io.InputStream;

public class ImageAsyncTask extends AsyncTask<String, Void, Bitmap> {

    private ImageView imageBitmap;

    public ImageAsyncTask(ImageView imageBitmap) {
        this.imageBitmap = imageBitmap;
    }

    protected Bitmap doInBackground(String... urls) {
        String url = urls[0];
        Bitmap mIcon11 = null;
        try {
            InputStream in = new java.net.URL(url).openStream();
            mIcon11 = BitmapFactory.decodeStream(in);
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            Log.e("Error", url);
            e.printStackTrace();
        }
        return mIcon11;
    }

    protected void onPostExecute(Bitmap result) {
        imageBitmap.setImageBitmap(result);
    }
}