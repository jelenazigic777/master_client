package rs.etf.navigation.gui.common;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import rs.etf.navigation.R;

import rs.etf.navigation.place.Item;

import static rs.etf.navigation.gui.Constants.ITEM;

public class AudioSingleActivity extends SimpleActivity implements AudioProgress.AudioProgressInterface {
    //todo proveriti sta se desava kada se uredjaj okrece
    //TODO Kada se uredjaj okrene obezbediti da se nastavi od mesta gde se stalo.

    public static final int NOT_SET = -1;
    public static final int STOP = 0;
    public static final int PLAY = 1;
    public static final int PAUSE = 2;

    public static final String AUDIO_STATE = "AUDIO_STATE";
    protected int audioState;

    public static final String PASSED_TIME = "PASSED_TIME";
    protected int passedTime;

    MediaPlayer mediaPlayer;
    int duration;

    Item item;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle extras = getIntent().getExtras();
        item = (Item) extras.getSerializable(ITEM);

        extractState(savedInstanceState);
        checkCenter();
        checkDescriptions();
        checkAudioAction();

    }

    @Override
    public int getActivityLayout() {
        return R.layout.activity_audio_single;
    }

    protected void extractState(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            audioState = savedInstanceState.getInt(AUDIO_STATE);
            passedTime = savedInstanceState.getInt(PASSED_TIME);
        } else {
            audioState = NOT_SET;
            passedTime = -1;
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt(AUDIO_STATE, audioState);
    }

    private void checkAudioAction() {
        if (audioState == NOT_SET || audioState == STOP) {
            start();
        }

        ImageView button = (ImageView) findViewById(R.id.play_stop);
        button.setOnClickListener(new AdapterView.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (audioState) {
                    case PLAY:
                        pause();
                        break;
                    case PAUSE:
                    default:
                        restart();
                        break;
                }
            }
        });
    }

    protected void checkCenter() {
        String url = item.getImageUrl();
        ImageView imageView = findViewById(R.id.centralImage);

        ImageLoader.loadAsync(this, imageView, url);
    }

    private void checkDescriptions() {
        TextView textTitle = (TextView) findViewById(R.id.textTitle);
        textTitle.setText(item.getName());

        TextView authorTitle = (TextView) findViewById(R.id.textAuthor);
        authorTitle.setText(item.getAuthor());

        TextView textDescription = (TextView) findViewById(R.id.textDescription);
        textDescription.setText(item.getText());
    }

    protected void stop() {
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
            passedTime = -1;
        }

        if (item != null) {
            audioState = STOP;
        } else {
            audioState = NOT_SET;
        }
        setPlayIcon();
        audioProgress.stopCountDownTimer();
    }

    protected void pause() {
        if (item != null) {
            if (audioState == PLAY) {
                try {
                    if (mediaPlayer.isPlaying()) {
                        passedTime = mediaPlayer.getCurrentPosition();
                        mediaPlayer.pause();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                stop();
            }
        }
        audioState = PAUSE;
        setPlayIcon();
    }

    protected void restart() {
        if (item != null) {
            if (audioState == PAUSE) {
                mediaPlayer.start();
                audioState = PLAY;
            } else {
                stop();
                audioState = STOP;
            }
        } else {
            audioState = STOP;
        }
        setPlayIcon();
    }

    protected void start() {
        if (item != null) {
            if (audioState != PLAY) {
                boolean pResult = play(item.getAudioUrl());
                audioProgress.startCountDownTimer();
                audioState = pResult ? PLAY : STOP;
            } else {
                if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                    audioState = PLAY;
                } else {
                    audioState = STOP;
                }
            }
        } else {
            audioState = NOT_SET;
        }
        setPlayIcon();
    }

    private void setPlayIcon() {
        ImageView button = (ImageView) findViewById(R.id.play_stop);
        switch (audioState) {
            case PLAY:
                button.setImageResource(R.drawable.ic_pause_circle_filled_black_24dp);
                break;
            case PAUSE:
                button.setImageResource(R.drawable.ic_play_circle_filled_black_24dp);
                break;
            default:
                button.setImageResource(R.drawable.ic_radio_button_checked_black_24dp);
                audioProgress.hideProgress();
                break;
        }
    }

    public boolean play(String url) {
        boolean result = true;
        try {
            if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                mediaPlayer.stop();
            }

            createMediaPlayer();

            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setDataSource(url);
            mediaPlayer.prepare();
            mediaPlayer.start();

            duration = mediaPlayer.getDuration();

        } catch (Exception e) {
            e.printStackTrace();
            result = false;
        }
        return result;

    }

    public void createMediaPlayer() {
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                stop();
            }
        });
        mediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                stop();
                Toast toast = Toast.makeText(AudioSingleActivity.this, getString(R.string.audio_error), Toast.LENGTH_LONG);
                return false;
            }
        });
    }

    @Override
    public void finish() {
        if (mediaPlayer != null) {
            mediaPlayer.release();
        }
        super.finish();
    }

    @Override
    public int getDuration() {
        return duration;
    }

    @Override
    public MediaPlayer getMediaPlayer() {
        return mediaPlayer;
    }

    AudioProgress audioProgress;

    @Override
    protected void extractLayout() {
        super.extractLayout();
        initViews();
    }

    private void initViews() {
        audioProgress = new AudioProgress(this, this);
    }

    //TODO proveriti sta se desava kada se izgubio audio fokus. Ovo je potrebno proveriti i na drugom mestu gde se pusta audio
}
