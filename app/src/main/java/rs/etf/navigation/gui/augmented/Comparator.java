package rs.etf.navigation.gui.augmented;

import android.support.v7.app.AppCompatActivity;

import org.opencv.calib3d.Calib3d;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.DMatch;
import org.opencv.core.KeyPoint;
import org.opencv.core.Mat;
import org.opencv.core.MatOfDMatch;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.features2d.DescriptorMatcher;
import org.opencv.features2d.ORB;

import java.util.ArrayList;
import java.util.List;

import rs.etf.navigation.place.Picture;

public class Comparator extends AppCompatActivity {

    private DescriptorMatcher mMatcher;
    private ORB mOrbDetector;

    MatOfKeyPoint keypointsScene = new MatOfKeyPoint();
    Mat descriptorsScene;
    Mat H;
    Mat imgObject;

    public void init() {
        mOrbDetector = ORB.create();
        mMatcher = DescriptorMatcher.create(DescriptorMatcher.BRUTEFORCE_HAMMING);
    }

    public void set(Mat mGray) {
        //camera frame
        Mat imgScene = mGray;

        //creation of keypoints, descriptors and feature detection
        keypointsScene = new MatOfKeyPoint();
        descriptorsScene = new Mat();
        mOrbDetector.detectAndCompute(imgScene, new Mat(), keypointsScene, descriptorsScene);

    }

    public boolean isEmpty() {
        return descriptorsScene.empty();
    }

    public boolean compare(Picture picture) {
        boolean result = false;

        imgObject = picture.getImgObject();
        Mat descriptorsObject = picture.getDescriptorsObject();
        MatOfKeyPoint keypointsObject = picture.getKeypointsObject();

        //matching features between imgScene and imgObject
        List<MatOfDMatch> knnMatches = new ArrayList<>();
        mMatcher.knnMatch(descriptorsObject, descriptorsScene, knnMatches, 2);

        //filtering good matches
        float ratioThresh = 0.6f;
        List<DMatch> listOfGoodMatches = new ArrayList<>();
        for (int i = 0; i < knnMatches.size(); i++) {
            if (knnMatches.get(i).rows() > 1) {
                DMatch[] matches = knnMatches.get(i).toArray();
                if (matches[0].distance < ratioThresh * matches[1].distance)
                    listOfGoodMatches.add(matches[0]);
            }
        }

        if (listOfGoodMatches.size() == 0)
            return result;

        MatOfDMatch goodMatches = new MatOfDMatch();
        goodMatches.fromList(listOfGoodMatches);

        //drawing matches
//            Mat imgMatches = new Mat();
//            Features2d.drawMatches(imgObject, keypointsObject, imgScene, keypointsScene, goodMatches, imgMatches, Scalar.all(-1),
//                    Scalar.all(-1), new MatOfByte(), Features2d.NOT_DRAW_SINGLE_POINTS);

        //localizing the object
        List<Point> obj = new ArrayList<>();
        List<Point> scene = new ArrayList<>();

        List<KeyPoint> listOfKeypointsObject = keypointsObject.toList();
        List<KeyPoint> listOfKeypointsScene = keypointsScene.toList();
        for (int i = 0; i < listOfGoodMatches.size(); i++) {
            obj.add(listOfKeypointsObject.get(listOfGoodMatches.get(i).queryIdx).pt);
            scene.add(listOfKeypointsScene.get(listOfGoodMatches.get(i).trainIdx).pt);
        }

        MatOfPoint2f objMat = new MatOfPoint2f();
        MatOfPoint2f sceneMat = new MatOfPoint2f();
        objMat.fromList(obj);
        sceneMat.fromList(scene);
        double ransacReprojThreshold = 3.0;
        H = Calib3d.findHomography(objMat, sceneMat, Calib3d.RANSAC, ransacReprojThreshold);

        if (H.empty())
            return result;
        result = true;
        return result;

    }

    public float[] getCorners() {
        try {//getting corners from imgObject
            Mat objCorners = new Mat(4, 1, CvType.CV_32FC2);
            Mat sceneCorners = new Mat();
            float[] objCornersData = new float[(int) (objCorners.total() * objCorners.channels())];
            objCorners.get(0, 0, objCornersData);
            objCornersData[0] = 0;
            objCornersData[1] = 0;
            objCornersData[2] = imgObject.cols();
            objCornersData[3] = 0;
            objCornersData[4] = imgObject.cols();
            objCornersData[5] = imgObject.rows();
            objCornersData[6] = 0;
            objCornersData[7] = imgObject.rows();
            objCorners.put(0, 0, objCornersData);

            Core.perspectiveTransform(objCorners, sceneCorners, H);
            float[] sceneCornersData = new float[(int) (sceneCorners.total() * sceneCorners.channels())];
            sceneCorners.get(0, 0, sceneCornersData);

            return sceneCornersData;
        } catch (Exception e) {
            return null;
        }
    }
}
