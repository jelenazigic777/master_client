package rs.etf.navigation.gui.common;

import android.content.Context;
import android.util.AttributeSet;
import android.support.v7.widget.*;
import android.widget.ImageView;

import rs.etf.navigation.R;

public class PlayButton extends AppCompatImageView {

    public static final int NOT_SET = -1;
    public static final int STOP = 0;
    public static final int PLAY = 1;
    public static final int PAUSE = 2;

    int state;
    int duration;

    public PlayButton(Context context) {
        super(context);
    }

    public PlayButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PlayButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    private void setPlayIcon() {
        switch (state) {
            case PLAY:
                setImageResource(R.drawable.ic_pause_circle_filled_black_24dp);
                break;
            case PAUSE:
                setImageResource(R.drawable.ic_play_circle_filled_black_24dp);
                break;
            default:
                setImageResource(R.drawable.ic_radio_button_checked_black_24dp);
                break;
        }
    }
}


