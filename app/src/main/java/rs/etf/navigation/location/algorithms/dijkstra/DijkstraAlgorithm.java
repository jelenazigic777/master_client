package rs.etf.navigation.location.algorithms.dijkstra;
//de.vogella.algorithms.dijkstra.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

//import de.vogella.algorithms.dijkstra.model.Edge;
//import de.vogella.algorithms.dijkstra.model.Graph;
//import de.vogella.algorithms.dijkstra.model.Vertex;

public class DijkstraAlgorithm<V> {

    private final List<V> nodes;
    private final List<Edge<V>> edges;
    private Set<V> settledNodes;
    private Set<V> unSettledNodes;
    private Map<V, V> predecessors;
    private Map<V, Integer> distance;

    public DijkstraAlgorithm(Graph graph) {
        // create a copy of the array so that we can operate on this array
        this.nodes = new ArrayList<V>(graph.getVes());
        this.edges = new ArrayList<Edge<V>>(graph.getEdges());
    }

    public void execute(V source) {
        settledNodes = new HashSet<V>();
        unSettledNodes = new HashSet<V>();
        distance = new HashMap<V, Integer>();
        predecessors = new HashMap<V, V>();
        distance.put(source, 0);
        unSettledNodes.add(source);
        while (unSettledNodes.size() > 0) {
            V node = getMinimum(unSettledNodes);
            settledNodes.add(node);
            unSettledNodes.remove(node);
            findMinimalDistances(node);
        }
    }

    private void findMinimalDistances(V node) {
        List<V> adjacentNodes = getNeighbors(node);
        for (V target : adjacentNodes) {
            if (getShortestDistance(target) > getShortestDistance(node)
                    + getDistance(node, target)) {
                distance.put(target, getShortestDistance(node)
                        + getDistance(node, target));
                predecessors.put(target, node);
                unSettledNodes.add(target);
            }
        }

    }

    private int getDistance(V node, V target) {
        for (Edge<V> edge : edges) {
            if (edge.getSource().equals(node)
                    && edge.getDestination().equals(target)) {
                return edge.getWeight();
            }
        }
        throw new RuntimeException("Should not happen");
    }

    private List<V> getNeighbors(V node) {
        List<V> neighbors = new ArrayList<V>();
        for (Edge<V> edge : edges) {
            if (edge.getSource().equals(node)
                    && !isSettled(edge.getDestination())) {
                neighbors.add(edge.getDestination());
            }
        }
        return neighbors;
    }

    private V getMinimum(Set<V> Ves) {
        V minimum = null;
        for (V V : Ves) {
            if (minimum == null) {
                minimum = V;
            } else {
                if (getShortestDistance(V) < getShortestDistance(minimum)) {
                    minimum = V;
                }
            }
        }
        return minimum;
    }

    private boolean isSettled(V V) {
        return settledNodes.contains(V);
    }

    private int getShortestDistance(V destination) {
        Integer d = distance.get(destination);
        if (d == null) {
            return Integer.MAX_VALUE;
        } else {
            return d;
        }
    }

    /*
     * This method returns the path from the source to the selected target and
     * NULL if no path exists
     */
    public LinkedList<V> getPath(V target) {
        LinkedList<V> path = new LinkedList<V>();
        V step = target;
        // check if a path exists
        if (predecessors.get(step) == null) {
            path.add(target);
            return path;
        }
        path.add(step);
        while (predecessors.get(step) != null) {
            step = predecessors.get(step);
            path.add(step);
        }
        // Put it into the correct order
        Collections.reverse(path);
        return path;
    }

}