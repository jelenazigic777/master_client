package rs.etf.navigation.location.algorithms.dijkstra;

import java.util.*;

import rs.etf.navigation.place.*;

public class PathFinder {

    public static List<Entry<Item, MuseumMap>> findPath(Museum museum, Item start, Item end) {
        List<Entry<Item, MuseumMap>> result = new LinkedList<>();

        List<Room> nodes = allRooms(museum);
        List<Edge<Room>> edges = allEdges(nodes);


        Graph graph = new Graph(nodes, edges);
        DijkstraAlgorithm dijkstra = new DijkstraAlgorithm(graph);

        Room startRoom = museum.getRoom(start);
        Room endRoom = museum.getRoom(end);

        dijkstra.execute(startRoom);
        LinkedList<Room> path = dijkstra.getPath(endRoom);

        for (Room room : path) {
            Entry<Item, MuseumMap> map = new Entry<>();
            MuseumMap museumMap = new MuseumMap();
            museumMap.load(room);
            museumMap.setFlorMap(room.getMap());
            museumMap.setItems(room.getItems());
            map.setKey(room);
            map.setValue(museumMap);
            result.add(map);
        }

        return result;
    }

    public static List<Edge<Room>> allEdges(List<Room> rooms) {
        List<Edge<Room>> edges = new LinkedList<>();
        int i = 0;
        for (Room room : rooms) {
            for (Room room2 : room.getConnected()) {
                Edge<Room> edge = new Edge<>("" + i, room, room2, 1);
                edges.add(edge);
            }
        }

        return edges;
    }

    public static List<Room> allRooms(Museum museum) {
        Set<Room> allRooms = new HashSet<>();
        List<Room> notSearchedRooms = new LinkedList<>();
        notSearchedRooms.addAll(museum.getRooms());
        while (notSearchedRooms.size() > 0) {
            Room a = notSearchedRooms.remove(0);
            if (!allRooms.contains(a)) {
                allRooms.add(a);
                notSearchedRooms.addAll(a.getConnected());
            }
        }

        List<Room> result = new LinkedList<>(allRooms);
        return result;

    }
}
