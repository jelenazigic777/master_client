package rs.etf.navigation.location.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.widget.Toast;

import rs.etf.navigation.R;

public class BluetoothLocationScanner extends ScanCallback {

    public interface LocationScanListener {
        void onScanResult(ScanResult scanResult);
    }

    private BluetoothLeScanner bluetoothLeScanner;
    private LocationScanListener locationScanListener;

    public BluetoothLocationScanner(Context context, LocationScanListener locationScanListener) {
        BluetoothManager bluetoothManager = (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
        BluetoothAdapter bluetoothAdapter = bluetoothManager.getAdapter();

        if (bluetoothAdapter == null || !bluetoothAdapter.isEnabled()) {
            Toast.makeText(context, context.getString(R.string.permission_denied_bluetooth), Toast.LENGTH_LONG).show();
            return;
        }

        bluetoothLeScanner = bluetoothAdapter.getBluetoothLeScanner();
        this.locationScanListener = locationScanListener;
    }

    public void scanLeDevices() {
        if (bluetoothLeScanner != null)
            bluetoothLeScanner.startScan(this);
    }

    public void stopScan() {
        if (bluetoothLeScanner != null) bluetoothLeScanner.stopScan(this);
    }

    @Override
    public void onScanResult(int callbackType, ScanResult result) {
        locationScanListener.onScanResult(result);
    }
}
