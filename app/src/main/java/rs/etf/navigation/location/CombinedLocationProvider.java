package rs.etf.navigation.location;

import android.content.Context;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import rs.etf.navigation.location.bluetooth.BluetoothLocationProvider;
import rs.etf.navigation.location.broadcast.LocationBroadcastReceiver;
import rs.etf.navigation.location.broadcast.GPSLocationBroadcastReceiver;
import rs.etf.navigation.location.sensors.AccMagLocationProvider;
import rs.etf.navigation.location.sensors.MagneticLocationProvider;
import rs.etf.navigation.place.Beacon;
import rs.etf.navigation.place.Museum;

public class CombinedLocationProvider implements LocationChangeListener, LocationProvider {

    private Museum museum;

    private BluetoothLocationProvider bluetoothLocationProvider;
    private LocationBroadcastReceiver broadcastLocationProvider;
    private GPSLocationBroadcastReceiver gpsLocationProvider;
    private AccMagLocationProvider accMagLocationProvider;

    private List<LocationChangeListener> locationListeners;
    private Location currentLocation;

    public CombinedLocationProvider(Museum museum) {
        this.museum = museum;

        this.locationListeners = (List<LocationChangeListener>) (new CopyOnWriteArrayList<LocationChangeListener>());
        this.bluetoothLocationProvider = new BluetoothLocationProvider();

        this.broadcastLocationProvider = new LocationBroadcastReceiver();

        this.gpsLocationProvider = new GPSLocationBroadcastReceiver();

        this.accMagLocationProvider = new AccMagLocationProvider();

        bluetoothLocationProvider.clearBeacons();
        for (Beacon beacon : museum.getBeacons()) {
            bluetoothLocationProvider.addBeacon(beacon);
        }
    }

    @Override
    public void onLocationChange(Location location) {
        this.currentLocation = location;
        for (LocationChangeListener listener : locationListeners) {
            listener.onLocationChange(location);
        }
    }

    boolean init = false;

    @Override
    public void init(Context context) {
        if (!init) {
            this.bluetoothLocationProvider.init(context);
            this.bluetoothLocationProvider.registerLocationChangeListener(this);

            this.broadcastLocationProvider.init(context);
            this.broadcastLocationProvider.registerLocationChangeListener(this);

            this.gpsLocationProvider.init(context);
            this.gpsLocationProvider.registerLocationChangeListener(this);

            this.accMagLocationProvider.init(context);
            this.accMagLocationProvider.registerLocationChangeListener(this);

            init = true;
        }
    }

    @Override
    public Location getLocation() {
        return currentLocation;
    }

    @Override
    public void registerLocationChangeListener(LocationChangeListener listener) {
        if (!locationListeners.contains(listener)) {
            locationListeners.add(listener);
        }
    }

    @Override
    public void unregisterLocationChangeListener(LocationChangeListener listener) {
        locationListeners.remove(listener);
    }
}
