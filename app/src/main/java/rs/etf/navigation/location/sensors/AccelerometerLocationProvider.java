package rs.etf.navigation.location.sensors;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import rs.etf.navigation.location.Location;
import rs.etf.navigation.location.LocationChangeListener;
import rs.etf.navigation.location.LocationProvider;

import static android.content.Context.SENSOR_SERVICE;

public class AccelerometerLocationProvider implements LocationProvider, SensorEventListener {
    //TODO nije zavrseno
    private Location currentLocation;
    private Handler handler;
    private List<LocationChangeListener> locationListeners;
    private SensorManager sensorManager;

    public AccelerometerLocationProvider() {
        this.currentLocation = null;
        this.handler = new Handler();
        locationListeners = (List<LocationChangeListener>) new CopyOnWriteArrayList<LocationChangeListener>();
    }

    @Override
    public void init(Context context) {
        sensorManager = (SensorManager) context.getSystemService(SENSOR_SERVICE);
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public Location getLocation() {
        return currentLocation;
    }


    @Override
    public void registerLocationChangeListener(LocationChangeListener listener) {
        if (!locationListeners.contains(listener)) {
            locationListeners.add(listener);
        }
    }

    @Override
    public void unregisterLocationChangeListener(LocationChangeListener listener) {
        locationListeners.remove(listener);
    }

    private float[] lastAccelerometer = new float[3];
    private float[] lastMagnetometer = new float[3];
    private float[] rotationMatrix = new float[9];
    private float[] orientation = new float[3];
    private float currentDegree = 0f;

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            System.arraycopy(event.values, 0, lastAccelerometer, 0, event.values.length);

            SensorManager.getRotationMatrix(rotationMatrix, null, lastAccelerometer, lastMagnetometer);
            SensorManager.getOrientation(rotationMatrix, orientation);
            float azimuthInRadians = orientation[0];
            currentDegree = -(float) (Math.toDegrees(azimuthInRadians) + 360) % 360;

            currentLocation.setViewingAngle(currentDegree);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
