package rs.etf.navigation.location.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.CopyOnWriteArrayList;

import rs.etf.navigation.gui.Constants;
import rs.etf.navigation.location.Location;
import rs.etf.navigation.location.LocationChangeListener;
import rs.etf.navigation.location.LocationProvider;

public class LocationBroadcastReceiver extends BroadcastReceiver implements LocationProvider {

    private Location currentLocation;
    private Handler handler;
    private List<LocationChangeListener> locationListeners;

    public LocationBroadcastReceiver() {
        this.currentLocation = null;
        this.handler = new Handler();
        locationListeners = (List<LocationChangeListener>) new CopyOnWriteArrayList<LocationChangeListener>();
    }

    @Override
    public void init(Context context) {
        IntentFilter filter = new IntentFilter(Constants.LOCATION_INTENT);
        context.registerReceiver(this, filter);
        //TODO registracija i deregistracija prijemnika nije uradjena ispravno
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        final Location location = (Location) intent.getSerializableExtra(Constants.LOCATION);
        currentLocation = location;

        handler.post(new Runnable() {
            @Override
            public void run() {
                for (LocationChangeListener listener : locationListeners) {
                    listener.onLocationChange(location);
                }

            }
        });
    }

    @Override
    public Location getLocation() {
        return currentLocation;
    }


    @Override
    public void registerLocationChangeListener(LocationChangeListener listener) {
        if (!locationListeners.contains(listener)) {
            locationListeners.add(listener);
        }
    }

    @Override
    public void unregisterLocationChangeListener(LocationChangeListener listener) {
        locationListeners.remove(listener);
    }
}
