package rs.etf.navigation.location.algorithms.dijkstra;
//package de.vogella.algorithms.dijkstra.model;

public class Edge<V>  {
    private final String id;
    private final V source;
    private final V destination;
    private final int weight;

    public Edge(String id, V source, V destination, int weight) {
        this.id = id;
        this.source = source;
        this.destination = destination;
        this.weight = weight;
    }

    public String getId() {
        return id;
    }
    public V getDestination() {
        return destination;
    }

    public V getSource() {
        return source;
    }
    public int getWeight() {
        return weight;
    }

    @Override
    public String toString() {
        return source + " " + destination;
    }


}