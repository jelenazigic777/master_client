package rs.etf.navigation.location.algorithms.dijkstra;
//package de.vogella.algorithms.dijkstra.model;

import java.util.List;

public class Graph<V> {
    private final List<V> Ves;
    private final List<Edge> Es;

    public Graph(List<V> Ves, List<Edge> Es) {
        this.Ves = Ves;
        this.Es = Es;
    }

    public List<V> getVes() {
        return Ves;
    }

    public List<Edge> getEdges() {
        return Es;
    }

}
