package rs.etf.navigation.location.broadcast;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.view.Gravity;
import android.widget.Toast;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import rs.etf.navigation.R;
import rs.etf.navigation.location.LocationChangeListener;
import rs.etf.navigation.location.LocationProvider;

public class GPSLocationBroadcastReceiver extends BroadcastReceiver implements LocationProvider {

    private rs.etf.navigation.location.Location currentLocationL;
    private Handler handlerL;
    private List<rs.etf.navigation.location.LocationChangeListener> locationListeners;

    public GPSLocationBroadcastReceiver() {
        this.currentLocationL = null;
        this.handlerL = new Handler();
        locationListeners = (List<rs.etf.navigation.location.LocationChangeListener>) new CopyOnWriteArrayList<LocationChangeListener>();
    }

    @Override
    public void init(Context context) {
//        IntentFilter filter = new IntentFilter(Constants.LOCATION_INTENT);
//        context.registerReceiver(this, filter);
    }

    public void updateLocation(Location locationGPS) {

        final rs.etf.navigation.location.Location location = convertGPSLocation(locationGPS);

        currentLocationL = location;

        handlerL.post(new Runnable() {
            @Override
            public void run() {

                for (rs.etf.navigation.location.LocationChangeListener listener : locationListeners) {
                    listener.onLocationChange(location);
                }

            }
        });
    }

    private rs.etf.navigation.location.Location convertGPSLocation(Location gps) {
        rs.etf.navigation.location.Location result = new rs.etf.navigation.location.Location();

        result.setX(gps.getLatitude());
        result.setY(gps.getLongitude());
        result.setZ(gps.getAltitude());
        result.setWeight(gps.getAccuracy());

        return result;
    }

    @Override
    public rs.etf.navigation.location.Location getLocation() {
        return currentLocationL;
    }


    @Override
    public void registerLocationChangeListener(rs.etf.navigation.location.LocationChangeListener listener) {
        if (!locationListeners.contains(listener)) {
            locationListeners.add(listener);
        }
    }

    @Override
    public void unregisterLocationChangeListener(rs.etf.navigation.location.LocationChangeListener listener) {
        locationListeners.remove(listener);
    }

    public static final int MIN_TIME_REQUEST = 5 * 1000;
    private static Location currentLocation;
    private static Location prevLocation;
    private static Context _context;
    private String provider = LocationManager.GPS_PROVIDER;
    private static Intent _intent;
    private static LocationManager locationManager;

    private LocationListener locationListener = new LocationListener() {

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            try {
                String strStatus = "";
                switch (status) {
                    case GpsStatus.GPS_EVENT_FIRST_FIX:
                        strStatus = "GPS_EVENT_FIRST_FIX";
                        break;
                    case GpsStatus.GPS_EVENT_SATELLITE_STATUS:
                        strStatus = "GPS_EVENT_SATELLITE_STATUS";
                        break;
                    case GpsStatus.GPS_EVENT_STARTED:
                        strStatus = "GPS_EVENT_STARTED";
                        break;
                    case GpsStatus.GPS_EVENT_STOPPED:
                        strStatus = "GPS_EVENT_STOPPED";
                        break;
                    default:
                        strStatus = String.valueOf(status);
                        break;
                }
                Toast.makeText(_context, _context.getString(R.string.status) + strStatus, Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onProviderDisabled(String provider) {
        }

        @Override
        public void onLocationChanged(Location location) {
            try {
                GPSLocationBroadcastReceiver.this.gotLocation(location);
            } catch (Exception e) {
            }
        }
    };

    // received request from the calling service
    @Override
    public void onReceive(final Context context, Intent intent) {
        //Toast.makeText(context, "new request received by receiver", Toast.LENGTH_SHORT).show();
        _context = context;
        _intent = intent;

        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        if (locationManager.isProviderEnabled(provider)) {
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            locationManager.requestLocationUpdates(provider, MIN_TIME_REQUEST, 5, locationListener);
            Location gotLoc = locationManager.getLastKnownLocation(provider);
            gotLocation(gotLoc);
        } else {
            Toast t = Toast.makeText(context, "please turn on GPS", Toast.LENGTH_LONG);
            t.setGravity(Gravity.CENTER, 0, 0);
            //t.show();
            Intent settingsIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            settingsIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            _context.startActivity(settingsIntent);
        }
    }

    private void gotLocation(Location location) {
        prevLocation = currentLocation == null ? null : new Location(currentLocation);
        currentLocation = location;
        if (isLocationNew()) {
            updateLocation(location);
            stopLocationListener();
        }
    }

    private static boolean isLocationNew() {
        if (currentLocation == null) {
            return false;
        } else if (prevLocation == null) {
            return true;
        } else if (currentLocation.getTime() >= prevLocation.getTime() + MIN_TIME_REQUEST / 2) {
            return false;
        } else {
            return true;
        }
    }

    public void stopLocationListener() {
        locationManager.removeUpdates(locationListener);
    }
}
