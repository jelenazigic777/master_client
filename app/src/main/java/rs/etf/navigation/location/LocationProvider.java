package rs.etf.navigation.location;

import android.content.Context;

public interface LocationProvider {

    void init(Context context);

    Location getLocation();

    void registerLocationChangeListener(LocationChangeListener listener);

    void unregisterLocationChangeListener(LocationChangeListener listener);

}
