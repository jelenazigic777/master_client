package rs.etf.navigation.location;

public interface LocationChangeListener {

    void onLocationChange(Location location);

}
