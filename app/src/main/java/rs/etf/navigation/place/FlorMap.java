package rs.etf.navigation.place;

import rs.etf.navigation.location.Location;

public class FlorMap extends Item {

    int florNumber;
    Location start, end;

    public FlorMap() {
        super();
        this.florNumber = 0;
        this.setType(Item.MAP);
    }

    public int getFlorNumber() {
        return florNumber;
    }

    public void setFlorNumber(int florNumber) {
        this.florNumber = florNumber;
    }

    public Location getStart() {
        return start;
    }

    public void setStart(Location start) {
        this.start = start;
    }

    public Location getEnd() {
        return end;
    }

    public void setEnd(Location end) {
        this.end = end;
    }
}
