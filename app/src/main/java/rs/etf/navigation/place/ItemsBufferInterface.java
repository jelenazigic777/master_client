package rs.etf.navigation.place;

import java.io.*;
import java.util.*;

import rs.etf.navigation.location.*;

interface ItemsBufferInterface extends Serializable {

    Item remove(Item item);

    Item remove(String itemName);

    Item remove(int itemId);

    Item getItem(String itemName);

    Item getItem(int itemId);

    Item getFirst();

    boolean add(Item item);

    boolean moveFirst(String itemName);

    boolean moveFirst(int itemId);

    List<Item> removeRoom(String room);

    List<Item> removeFromLocation(Location location);

    List<Item> removeFromLocation(Location location, double distance);

    boolean setVisited(Item item);

    boolean setVisited(String itemName);

    boolean setVisited(int itemId);

    boolean setNotVisited(Item item);

    boolean setNotVisited(String itemName);

    boolean setNotVisited(int itemId);

    List<Item> removeAllVisited();

    List<Item> getAllVisited();

    List<Item> getAll();

    List<Item> getAllUnvisited();
}
