package rs.etf.navigation.place;

import java.util.*;

import rs.etf.navigation.location.*;

public class ItemsBuffer implements ItemsBufferInterface {

    List<Item> items;

    @Override
    public Item remove(Item item) {
        return item;
    }

    @Override
    public Item remove(String itemName) {
        return null;
    }

    @Override
    public Item remove(int itemId) {
        return null;
    }

    @Override
    public Item getItem(String itemName) {
        return null;
    }

    @Override
    public Item getItem(int itemId) {
        return null;
    }

    @Override
    public Item getFirst() {
        return null;
    }

    @Override
    public boolean add(Item item) {
        return false;
    }

    @Override
    public boolean moveFirst(String itemName) {
        return false;
    }

    @Override
    public boolean moveFirst(int itemId) {
        return false;
    }

    @Override
    public List<Item> removeRoom(String room) {
        return null;
    }

    @Override
    public List<Item> removeFromLocation(Location location) {
        return null;
    }

    @Override
    public List<Item> removeFromLocation(Location location, double distance) {
        return null;
    }

    @Override
    public boolean setVisited(Item item) {
        return false;
    }

    @Override
    public boolean setVisited(String itemName) {
        return false;
    }

    @Override
    public boolean setVisited(int itemId) {
        return false;
    }

    @Override
    public boolean setNotVisited(Item item) {
        return false;
    }

    @Override
    public boolean setNotVisited(String itemName) {
        return false;
    }

    @Override
    public boolean setNotVisited(int itemId) {
        return false;
    }

    @Override
    public List<Item> removeAllVisited() {
        return null;
    }

    @Override
    public List<Item> getAllVisited() {
        return null;
    }

    @Override
    public List<Item> getAll() {
        return null;
    }

    @Override
    public List<Item> getAllUnvisited() {
        return null;
    }
}
