package rs.etf.navigation.place;

import java.util.LinkedList;
import java.util.List;

import rs.etf.navigation.location.Location;

public class Museum extends Item {

    private List<Item> items;
    private List<FlorMap> maps;
    private List<Exhibition> exhibitions;
    private List<Room> rooms;
    private List<MuseumMap> museumMaps;
    private List<Beacon> beacons;
    private String primaryImage;
    private String secondaryImage;

    public Museum() {
        super();
        this.setType(Item.MUSEUM);
        this.beacons = new LinkedList<>();
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public List<FlorMap> getMaps() {
        return maps;
    }

    public void setMaps(List<FlorMap> maps) {
        this.maps = maps;
    }

    public List<Exhibition> getExhibitions() {
        return exhibitions;
    }

    public void setExhibitions(List<Exhibition> exhibitions) {
        this.exhibitions = exhibitions;
    }

    public List<Room> getRooms() {
        return rooms;
    }

    public void setRooms(List<Room> rooms) {
        this.rooms = rooms;
    }

    public List<MuseumMap> getMuseumMaps() {
        return museumMaps;
    }

    public void setMuseumMaps(List<MuseumMap> museumMaps) {
        this.museumMaps = museumMaps;
    }

    public List<Beacon> getBeacons() {
        return beacons;
    }

    public void setBeacons(List<Beacon> beacons) {
        this.beacons = beacons;
    }

    public String getPrimaryImage() {
        return primaryImage;
    }

    public void setPrimaryImage(String primaryImage) {
        this.primaryImage = primaryImage;
    }

    public String getSecondaryImage() {
        return secondaryImage;
    }

    public void setSecondaryImage(String secondaryImage) {
        this.secondaryImage = secondaryImage;
    }

    public List<Item> getItems(FlorMap map) {
        List<Item> result = new LinkedList<>();
        if (museumMaps != null) {
            for (MuseumMap museumMap : museumMaps) {
                if (museumMap != null && museumMap.getFlorMap() == map) {
                    List<Item> tmp = museumMap.getItems();
                    if (tmp != null) {
                        result.addAll(tmp);
                    }
                }
            }
        }
        return result;
    }

    public List<FlorMap> getFlorMaps() {
        List<FlorMap> result = new LinkedList<>();
        if (museumMaps != null) {
            for (MuseumMap museumMap : museumMaps) {
                if (museumMap != null) {
                    FlorMap tmp = museumMap.getFlorMap();
                    if (tmp != null) {
                        result.add(tmp);
                    }
                }
            }
        }
        return result;
    }

    public FlorMap getNextFlorMap(FlorMap currentFlor) {
        FlorMap result = null;
        List<FlorMap> flores = getMaps();
        int currentFlorNumber = currentFlor != null ? currentFlor.getFlorNumber() : Integer.MAX_VALUE;

        int min = Integer.MAX_VALUE;

        for (FlorMap flor : flores) {
            int tempFlorNumber = flor != null ? flor.getFlorNumber() : 0;
            if (tempFlorNumber > currentFlorNumber && tempFlorNumber < min) {
                min = tempFlorNumber;
                result = flor;
            }
        }
        if (result != null) {
            currentFlor = result;
        }
        return result;
    }

    public FlorMap getMuseumMap(FlorMap currentFlor) {
        FlorMap result = null;
        List<FlorMap> flores = getMaps();
        int currentFlorNumber = currentFlor != null ? currentFlor.getFlorNumber() : Integer.MIN_VALUE;

        int min = Integer.MIN_VALUE;

        for (FlorMap flor : flores) {
            int tempFlorNumber = flor != null ? flor.getFlorNumber() : 0;
            if (tempFlorNumber < currentFlorNumber && tempFlorNumber > min) {
                min = tempFlorNumber;
                result = flor;
            }
        }
        if (result != null) {
            currentFlor = result;
        }
        return result;
    }

    public MuseumMap getMuseumMap(Location location) {
        MuseumMap result = null;
        List<MuseumMap> flores = getMuseumMaps();

        double min = Double.MAX_VALUE;

        for (MuseumMap flor : flores) {
            double tmp = Math.abs(flor.getLocation().getZ() - location.getZ());
            if (tmp < min) {
                min = tmp;
                result = flor;
            }
        }
        return result;
    }

    public Room getRoom(Item item) {
        for (Room room : getRooms()) {
            if (room != null && (room.getItems() != null && room.getItems().contains(item) || room == item)) {
                return room;
            }
        }
        return rooms.get(0);
    }

    public Room getRoom(Location location) {
        for (Room room : getRooms()) {
            if (room != null && room.isIn(location)) {
                return room;
            }
        }
        return rooms.get(0);
    }

}
