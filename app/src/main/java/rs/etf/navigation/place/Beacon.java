package rs.etf.navigation.place;

import java.util.UUID;

import rs.etf.navigation.location.Location;

public class Beacon extends Item {

    private UUID uuid;
    private int major, minor, power;

    public Beacon() {
        super();
        this.setType(Item.BEACON);
    }

    public Beacon(Location location, UUID uuid, int major, int minor, int power) {
        this();
        this.setLocation(location);
        this.uuid = uuid;
        this.major = major;
        this.minor = minor;
        this.power = power;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public int getMajor() {
        return major;
    }

    public void setMajor(int major) {
        this.major = major;
    }

    public int getMinor() {
        return minor;
    }

    public void setMinor(int minor) {
        this.minor = minor;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }
}
