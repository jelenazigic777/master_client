package rs.etf.navigation.place;

import java.io.Serializable;

public class Entry<T, V> implements Serializable {
    T key;
    V value;

    public Entry() {
        this.key = null;
        this.value = null;
    }

    public Entry(T key, V value) {
        this.key = key;
        this.value = value;
    }

    public T getKey() {
        return key;
    }

    public void setKey(T key) {
        this.key = key;
    }

    public V getValue() {
        return value;
    }

    public void setValue(V value) {
        this.value = value;
    }
}