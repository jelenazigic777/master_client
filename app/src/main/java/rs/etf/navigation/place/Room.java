package rs.etf.navigation.place;

import java.util.LinkedList;
import java.util.List;

import rs.etf.navigation.location.Location;

public class Room extends Item {

    private FlorMap map;

    private List<Item> items;

    private List<Room> connected;

    private List<ConstructionElement> constructionElements;

    public Room() {
        super();
        this.setType(Item.ROOM);
        constructionElements = new LinkedList<>();
    }

    public FlorMap getMap() {
        return map;
    }

    public void setMap(FlorMap map) {
        this.map = map;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public List<Room> getConnected() {
        return connected;
    }

    public void setConnected(List<Room> connected) {
        this.connected = connected;
    }

    public List<ConstructionElement> getConstructionElements() {
        return constructionElements;
    }

    public void setConstructionElements(List<ConstructionElement> constructionElements) {
        this.constructionElements = constructionElements;
    }

    public boolean isIn(Location location) {
        if (this.getLocation() == null || location == null) return false;
        return this.getLocation().isAt(location);
        //TODO uracunati udaljenost koristeci konstrukcione elemente
    }

}
