package rs.etf.navigation.place;

import android.graphics.Point;

public class ConstructionElement implements Comparable<ConstructionElement> {
    protected Point start, end;
    protected int type;

    public ConstructionElement() {
        this.type = NOT_SET;
    }

    public ConstructionElement(Point start, Point end, int type) {
        this.start = start;
        this.end = end;
        this.type = type;
    }

    public Point getStart() {
        return start;
    }

    public void setStart(Point start) {
        this.start = start;
    }

    public Point getEnd() {
        return end;
    }

    public void setEnd(Point end) {
        this.end = end;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public static final int NOT_SET = 0;
    public static final int WALL = 1;
    public static final int DOOR = 2;
    public static final int WINDOW = 3;

    @Override
    public int compareTo(ConstructionElement element) {
        if ((this.start.x == element.start.x && this.start.y == element.start.y
                && this.end.x == element.end.x && this.end.y == element.end.y)
                || (this.start.x == element.end.x && this.start.y == element.end.y
                && this.end.x == element.start.x && this.end.y == element.start.y))
            return 0;
        else {
            return this.start.x < element.start.x ? 1 : -1;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof ConstructionElement) {
            ConstructionElement element = (ConstructionElement) o;
            return (this.start.x == element.start.x && this.start.y == element.start.y
                    && this.end.x == element.end.x && this.end.y == element.end.y)
                    || (this.start.x == element.end.x && this.start.y == element.end.y
                    && this.end.x == element.start.x && this.end.y == element.start.y);
        } else return false;
    }
}
