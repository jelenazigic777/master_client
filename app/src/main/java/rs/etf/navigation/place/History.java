package rs.etf.navigation.place;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class History<T> implements Serializable {

    private List<Entry<T, Long>> history;
    private long interval;
    private long number;

    public History() {
        this.history = new LinkedList<>();
        this.interval = Long.MAX_VALUE;
        this.number = Integer.MAX_VALUE;
    }

    public boolean check(T data, long time) {
        boolean result = true;

        if (data != null) {
            int i = 0;
            for (Entry<T, Long> val : history) {
                if (i++ >= number) break;
                T prevData = val.key;
                long prevTime = val.value;
                if (Math.abs(time - prevTime) > interval) break;
                if (prevData == data || prevData.equals(data)) {
                    result = false;
                    break;
                }
            }
        } else {
            result = false;
        }
        return result;
    }

    public void add(T data, long time) {
        if (data == null) return;
        Entry<T, Long> val = new Entry<>(data, time);

        history.add(val);
    }

    public long getInterval() {
        return interval;
    }

    public void setInterval(long interval) {
        this.interval = interval;
    }

    public long getNumber() {
        return number;
    }

    public void setNumber(long number) {
        this.number = number;
    }
}
