package rs.etf.navigation.place;

import java.io.*;

import rs.etf.navigation.location.*;

public class Item implements Serializable {

    private String id;

    private String institution;
    private int type;
    private String language;
    private String name;
    private String author;
    private String text;

    private String textUrl;
    private String textUrlLocal;

    private String imageUrl;
    private String imageUrlLocal;

    private String audioUrl;
    private String audioUrlLocal;
    private int audioType;

    private String videoUrl;
    private String videoUrlLocal;

    private Location location;
    private double radius;
    private String room;

    private String[] additionalImagesUrl;
    private String[] additionalImagesUrlLocal;

    private String qr;

    public Item() {
        this.id = null;
        this.institution = null;
        this.type = NOT_ITEM;
        this.language = null;
        this.name = null;
        this.author = null;
        this.text = null;

        this.textUrl = null;
        this.textUrlLocal = null;

        this.imageUrl = null;
        this.imageUrlLocal = null;

        this.audioUrl = null;
        this.audioUrlLocal = null;
        this.audioType = NOT_AUDIO;

        this.videoUrl = null;
        this.videoUrlLocal = null;

        this.location = null;
        this.radius = 0;
        this.room = null;

        this.additionalImagesUrl = null;
        this.additionalImagesUrlLocal = null;
    }

    public Item(String id, String institution, int type, String language, String name, String author, String text, String textUrl, String imageUrl, String audioUrl, int audioType, String videoUrl, Location location, double radius, String room, String additionalImagesUrl) {
        this.id = id;
        this.institution = institution;
        this.type = type;
        this.language = language;
        this.name = name;
        this.author = author;
        this.text = text;

        this.textUrl = textUrl;
        this.textUrlLocal = null;

        this.imageUrl = imageUrl;
        this.imageUrlLocal = null;

        this.audioUrl = audioUrl;
        this.audioUrlLocal = null;
        this.audioType = audioType;

        this.videoUrl = videoUrl;
        this.videoUrlLocal = null;

        this.location = location;
        this.radius = radius;
        this.room = room;

        this.setAdditionalImagesUrls(additionalImagesUrl);
        this.additionalImagesUrlLocal = null;

        this.qr = id;//null
    }

    public Item(Item item) {
        this.id = item.id;
        this.institution = item.institution;
        this.type = item.type;
        this.language = item.language;
        this.name = item.name;
        this.author = item.author;
        this.text = item.text;

        this.textUrl = item.textUrl;
        this.textUrlLocal = item.textUrlLocal;

        this.imageUrl = item.imageUrl;
        this.imageUrlLocal = item.imageUrlLocal;

        this.audioUrl = item.audioUrl;
        this.audioUrlLocal = item.audioUrlLocal;
        this.audioType = item.audioType;

        this.videoUrl = item.videoUrl;
        this.videoUrlLocal = item.videoUrlLocal;

        this.location = item.location;
        this.radius = item.radius;
        this.room = item.room;

        this.additionalImagesUrl = item.additionalImagesUrl;
        this.additionalImagesUrlLocal = item.additionalImagesUrlLocal;

        this.qr = item.qr;
    }

    public Item(String id, String institution, int type, String language, String name, String author, String text, String textUrl, String textUrlLocal, String imageUrl, String imageUrlLocal, String audioUrl, String audioUrlLocal, int audioType, String videoUrl, String videoUrlLocal, Location location, double radius, String room, String additionalImagesUrls, String additionalImagesUrlsLocal, String qr) {
        this.id = id;
        this.institution = institution;
        this.type = type;
        this.language = language;
        this.name = name;
        this.author = author;
        this.text = text;

        this.textUrl = textUrl;
        this.textUrlLocal = textUrlLocal;

        this.imageUrl = imageUrl;
        this.imageUrlLocal = imageUrlLocal;

        this.audioUrl = audioUrl;
        this.audioUrlLocal = audioUrlLocal;
        this.audioType = audioType;

        this.videoUrl = videoUrl;
        this.videoUrlLocal = videoUrlLocal;

        this.location = location;
        this.radius = radius;
        this.room = room;

        this.setAdditionalImagesUrls(additionalImagesUrls);
        this.setAdditionalImagesUrlsLocal(additionalImagesUrlsLocal);

        this.qr = qr;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInstitution() {
        return institution;
    }

    public void setInstitution(String institution) {
        this.institution = institution;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTextUrl() {
        return textUrl;
    }

    public void setTextUrl(String textUrl) {
        this.textUrl = textUrl;
    }

    public String getTextUrlLocal() {
        return textUrlLocal;
    }

    public void setTextUrlLocal(String textUrlLocal) {
        this.textUrlLocal = textUrlLocal;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getImageUrlLocal() {
        return imageUrlLocal;
    }

    public void setImageUrlLocal(String imageUrlLocal) {
        this.imageUrlLocal = imageUrlLocal;
    }

    public String getAudioUrl() {
        return audioUrl;
    }

    public void setAudioUrl(String audioUrl) {
        this.audioUrl = audioUrl;
    }

    public String getAudioUrlLocal() {
        return audioUrlLocal;
    }

    public void setAudioUrlLocal(String audioUrlLocal) {
        this.audioUrlLocal = audioUrlLocal;
    }

    public int getAudioType() {
        return audioType;
    }

    public void setAudioType(int audioType) {
        this.audioType = audioType;
    }

    public Location getLocation() {
        return location;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getVideoUrlLocal() {
        return videoUrlLocal;
    }

    public void setVideoUrlLocal(String videoUrlLocal) {
        this.videoUrlLocal = videoUrlLocal;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String[] getAdditionalImagesUrl() {
        return additionalImagesUrl;
    }

    public void setAdditionalImagesUrl(String[] additionalImagesUrl) {
        this.additionalImagesUrl = additionalImagesUrl;
    }

    public String[] getAdditionalImagesUrlLocal() {
        return additionalImagesUrlLocal;
    }

    public void setAdditionalImagesUrlLocal(String[] additionalImagesUrlLocal) {
        this.additionalImagesUrlLocal = additionalImagesUrlLocal;
    }


    public void setAdditionalImagesUrls(String additionalImagesUrls) {
        this.additionalImagesUrl = extractStrings(additionalImagesUrls);
    }

    public String getAdditionalImagesUrls() {
        return extractString(additionalImagesUrl);
    }

    public void setAdditionalImagesUrlsLocal(String additionalImagesUrlsLocal) {
        this.additionalImagesUrlLocal = extractStrings(additionalImagesUrlsLocal);
    }

    public String getAdditionalImagesUrlsLocal() {
        return extractString(additionalImagesUrlLocal);
    }

    public String getQr() {
        return qr;
    }

    public void setQr(String qr) {
        this.qr = qr;
    }

    private String[] extractStrings(String data) {
        String[] result = data == null ? null : data.split(", ");
        return result;
    }

    private String extractString(String[] data) {
        StringBuilder sb = new StringBuilder();
        if (data != null) {
            for (int i = 0; i < data.length; i++) {
                sb.append(data[i]);
                if (i != data.length - 1) {
                    sb.append(", ");
                }
            }
        }
        return sb.toString();
    }

    public boolean containsDetails() {
        return textUrl != null && textUrl.length() > 0;
    }

    public boolean containsImage() {
        return imageUrl != null && imageUrl.length() > 0;
    }

    public boolean containsAudio() {
        return audioUrl != null && audioUrl.length() > 0;
    }

    public boolean containsAudioLocal() {
        return audioUrlLocal != null && audioUrlLocal.length() > 0;
    }

    public boolean containsVideo() {
        return videoUrl != null && videoUrl.length() > 0;
    }

    public boolean containsAdditionalImages() {
        return additionalImagesUrl != null && additionalImagesUrl.length > 0 && additionalImagesUrl[0].length() > 0;
    }

    public boolean containsContent() {
        return containsDetails() || containsImage() || containsAudio() || containsVideo();
    }

    public boolean containsIAVContent() {
        return containsImage() || containsAudio() || containsVideo();
    }

    public boolean containsLocation() {
        return location != null;
    }

    public void load(Item item) {
        this.id = item.id;
        this.institution = item.institution;
        this.type = item.type;
        this.language = item.language;
        this.name = item.name;
        this.author = item.author;
        this.text = item.text;

        this.textUrl = item.textUrl;
        this.textUrlLocal = item.textUrlLocal;

        this.imageUrl = item.imageUrl;
        this.imageUrlLocal = item.imageUrlLocal;

        this.audioUrl = item.audioUrl;
        this.audioUrlLocal = item.audioUrlLocal;
        this.audioType = item.audioType;

        this.videoUrl = item.videoUrl;
        this.videoUrlLocal = item.videoUrlLocal;

        this.location = item.location;
        this.radius = item.radius;
        this.room = item.room;

        this.additionalImagesUrl = item.additionalImagesUrl;
        this.additionalImagesUrlLocal = item.additionalImagesUrlLocal;

        this.qr = item.qr;
    }


    public Item copy() {
        Item result = new Item();
        result.load(this);
        return result;
    }

    public static final int NOT_AUDIO = 0;
    public static final int AUDIO = 1;
    public static final int REPETITIVE_AUDIO = 2;

    public static final int NOT_ITEM = 0;
    public static final int PICTURE = 1;
    public static final int SCULPTURE = 2;
    public static final int EXHIBITION = 3;
    public static final int ANNOUNCEMENT = 4;
    public static final int MAP = 5;
    public static final int MUSEUM = 6;
    public static final int ROOM = 7;
    public static final int ELEVATOR = 8;
    public static final int STAIRCASE_UP = 9;
    public static final int STAIRCASE_DOWN = 10;
    public static final int CURRENT_LOCATION = 11;
    public static final int MUSEUM_MAP = 12;
    public static final int DIRECTION_LEFT = 13;
    public static final int DIRECTION_RIGHT = 14;
    public static final int BEACON = 15;

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + ": " + getId();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (obj == this) return true;
        return this.id.equals(((Item) obj).getId());
    }
}
