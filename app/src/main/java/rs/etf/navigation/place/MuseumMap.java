package rs.etf.navigation.place;

import java.util.LinkedList;
import java.util.List;

public class MuseumMap extends Item {

    private FlorMap florMap;
    private List<Item> items;
    private List<Beacon> beacons;

    public MuseumMap() {
        super();
        this.setType(Item.MUSEUM_MAP);
        this.items = new LinkedList<>();
    }

    public FlorMap getFlorMap() {
        return florMap;
    }

    public void setFlorMap(FlorMap florMap) {
        this.florMap = florMap;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public List<Beacon> getBeacons() {
        return beacons;
    }

    public void setBeacons(List<Beacon> beacons) {
        this.beacons = beacons;
    }

    public List<Entry<Item, MuseumMap>> getItemsMaps() {
        List<Entry<Item, MuseumMap>> result = new LinkedList<>();
        for (Item item : getItems()) {
            Entry<Item, MuseumMap> itemsMap = new Entry<>(item, this);
            result.add(itemsMap);
        }
        return result;
    }

    @Override
    public void load(Item item) {
        super.load(item);
        if (item instanceof MuseumMap) {
            MuseumMap map = (MuseumMap) item;
            this.florMap = map.florMap;
            this.items = map.items;
            this.beacons = map.beacons;
        } else if (item instanceof Room) {
            Room room = (Room) item;
            this.florMap = room.getMap();
            this.items = room.getItems();
        }
    }

    @Override
    public boolean containsContent() {
        return florMap != null;// && super.containsDetails();
    }

}
