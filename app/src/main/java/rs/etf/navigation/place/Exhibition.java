package rs.etf.navigation.place;

import java.util.*;

public class Exhibition extends Item {

    private List<Item> items;
    private List<MuseumMap> maps;

    public Exhibition() {
        super();
        this.setType(Item.EXHIBITION);
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public List<MuseumMap> getMuseumMaps() {
        return maps;
    }

    public void setMuseumMaps(List<MuseumMap> maps) {
        this.maps = maps;
    }

    public List<Entry<Item, MuseumMap>> getItemsMaps() {
        List<Entry<Item, MuseumMap>> result = new LinkedList<>();
        for (MuseumMap map : maps) {
            for (Item item : map.getItems()) {
                Entry<Item, MuseumMap> itemsMap = new Entry<>(item, map);
                result.add(itemsMap);
            }
        }
        return result;
    }

    @Override
    public boolean containsContent() {
        return items != null && items.size() > 0;
    }
}
