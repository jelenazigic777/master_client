package rs.etf.navigation.place;

public class Sculpture extends Picture {

    public Sculpture() {
        super();
        this.setType(Item.SCULPTURE);
    }

}
