package rs.etf.navigation.place;

import org.opencv.core.Mat;
import org.opencv.core.MatOfKeyPoint;

public class Picture extends Item {

    public Picture() {
        super();
        this.imgObject = null;
        this.keypointsObject = null;
        this.descriptorsObject = null;
        this.setType(Item.PICTURE);
    }

    public Picture(Item item) {
        super(item);
        this.imgObject = null;
        this.keypointsObject = null;
        this.descriptorsObject = null;
        this.setType(Item.PICTURE);
    }

    private transient Mat imgObject;

    private transient MatOfKeyPoint keypointsObject;

    private transient Mat descriptorsObject;

    public Mat getImgObject() {
        return imgObject;
    }

    public void setImgObject(Mat imgObject) {
        this.imgObject = imgObject;
    }

    public MatOfKeyPoint getKeypointsObject() {
        return keypointsObject;
    }

    public void setKeypointsObject(MatOfKeyPoint keypointsObject) {
        this.keypointsObject = keypointsObject;
    }

    public Mat getDescriptorsObject() {
        return descriptorsObject;
    }

    public void setDescriptorsObject(Mat descriptorsObject) {
        this.descriptorsObject = descriptorsObject;
    }

}
