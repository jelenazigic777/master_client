package rs.etf.navigation.db;

import java.util.*;

import rs.etf.navigation.location.*;
import rs.etf.navigation.place.*;

public class DataProvider {

    static {
        init();
    }

    public static void init() {
        //museum = TestData2.createMuseum();
        museum = TestData3.createMuseum();
        allItems = museum.getItems();
        locationProvider = new CombinedLocationProvider(museum);
    }

    static List<Item> allItems;

    public static List<Item> getAllItems() {
        List<Item> result = new LinkedList<>();
        result.addAll(allItems);
        return result;
    }

    public static void setAllItems(List<Item> items) {
        allItems = items;
    }

    public static Item getItem(String id) {
        Item result = null;
        for (Item item : allItems) {
            if (item.getId().equals(id)) {
                result = item;
                break;
            }
        }
        return result;
    }

    public static Item getItemQR(String qr) {
        Item result = null;
        for (Item item : allItems) {
            if (item.getQr().equals(qr)) {
                result = item;
                break;
            }
        }
        return result;

    }

    public static List<Item> getAllExhibitions() {
        List<Item> result = new LinkedList<>();
        if (museum != null && museum.getExhibitions() != null) {
            result.addAll(museum.getExhibitions());
        }
        return result;
    }

    public static Item getExhibition(String id) {
        Item result = new Item();

        return result;
    }

    private static List<Item> visitedItems = new LinkedList<>();

    public static List<Item> getVisitedItems() {
        return visitedItems;
    }

    public static void addVisitedItem(Item item) {
        if (!visitedItems.contains(item)) {
            visitedItems.add(item);
        }
    }

    protected static List<Picture> pictures;

    public static List<Picture> getPictures() {
        return pictures;
    }

    public static void setPictures(List<Picture> pictures) {
        DataProvider.pictures = pictures;
    }

    public static Picture getPicture(String id) {
        Picture result = null;
        for (Picture item : pictures) {
            if (item.getId().equals(id)) {
                result = item;
                break;
            }
        }
        return result;
    }

    static Museum museum;

    public static Museum getMuseum() {
        return museum;
    }

    public static void setMuseum(Museum museum) {
        DataProvider.museum = museum;
    }

    public static List<MuseumMap> getMuseumMaps() {
        return museum.getMuseumMaps();
    }

    public static void setMuseumMap(List<MuseumMap> museumMaps) {
        museum.setMuseumMaps(museumMaps);
    }

    static LocationProvider locationProvider;

    public static LocationProvider getLocationProvider() {
        return locationProvider;
    }


    public static List<MuseumMap> getDirections(Location location, Item destination) {
        List<MuseumMap> result = museum.getMuseumMaps();

        return result;
    }

    public static List<MuseumMap> getDirections(Item item) {
        List<MuseumMap> result = museum.getMuseumMaps();

        return result;
    }

    public static MuseumMap getMap(Item search) {
        MuseumMap result = new MuseumMap();

        if (search != null) {
            List<MuseumMap> mMaps = museum.getMuseumMaps();
            for (MuseumMap mMap : mMaps) {
                for (Item item : mMap.getItems()) {
                    if (search.getId().equals(item.getId())) {
                        result.setFlorMap(mMap.getFlorMap());
                        List<Item> items = new LinkedList<>();
                        items.addAll(mMap.getItems());
                        result.setItems(items);
                        break;
                    }
                }
            }
        }

        return result;
    }


    public static List<Item> findItems(Location location) {
        List<Item> result = new LinkedList<>();
        if (location != null) {
            for (Item item : allItems) {
                Location itemsLocation = item.getLocation();
                double radius = item.getRadius();
                if (itemsLocation != null && areClose(itemsLocation, location, radius)) {
                    result.add(item);
                }
            }
        }
        return result;
    }

    private static boolean areClose(Location itemsLocation, Location location, double radius) {
        double distance = itemsLocation.distance(location);
        boolean result = distance < location.getWeight() + radius;
        return result;
    }
}
