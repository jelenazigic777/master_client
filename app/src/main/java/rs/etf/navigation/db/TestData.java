package rs.etf.navigation.db;

import android.support.annotation.NonNull;

import java.util.LinkedList;
import java.util.List;

import rs.etf.navigation.location.Location;
import rs.etf.navigation.place.Exhibition;
import rs.etf.navigation.place.FlorMap;
import rs.etf.navigation.place.Item;
import rs.etf.navigation.place.Museum;
import rs.etf.navigation.place.MuseumMap;
import rs.etf.navigation.place.Room;
import rs.etf.navigation.place.Staircase;

public class TestData {

    public static List<Item> createItems() {
        List<Item> result = new LinkedList<>();
        Item item = new Item();
        item.setId("1");
        item.setName("Портрет и аутопортрет");
        item.setAuthor("Љуба Поповић");
        item.setText("Техника: уље на платну Година: 2000 Димензије у mm: 565 x 513");
        item.setTextUrl("http://www.mgva.rs/slika.php?id=1");
        item.setImageUrl("http://www.mgva.rs/slike/ljuba/ljuba_2902.jpg");
        item.setVideoUrl("https://www.youtube.com/watch?v=sBSqOZz8PqQ");
        item.setAudioUrl("http://rti.etf.bg.ac.rs/rti/ri3aor/Simulators/museum/LjubaPopovic.mp3");
        String extra = "http://www.mgva.rs/slike/ljuba/ljuba_3191.jpg, http://www.mgva.rs/slike/ljuba/ljuba_2997.jpg, http://www.mgva.rs/slike/ljuba/ljuba_2996.jpg";
        item.setAdditionalImagesUrls(extra);
        item.setLocation(new Location(50, 60, 0));
        result.add(item);

        item = new Item();
        item.setId("2");
        item.setName("Завештање");
        item.setAuthor("Милић од Мачве");
        item.setText("2002/1");
        item.setTextUrl("http://www.mgva.rs/katalog.php?id=331");
        item.setImageUrl("http://www.mgva.rs/kat/KMGV%20Mili%C4%87%20od%20Ma%C4%8Dve%20-%20Zave%C5%A1tanje.jpg");
        item.setVideoUrl("https://www.youtube.com/watch?v=ehwJe4-7QKU");
        item.setAudioUrl("http://rti.etf.bg.ac.rs/rti/ri3aor/Simulators/museum/MilicodMacve.mp3");
        item.setLocation(new Location(10, 20, 0));
        result.add(item);

        item = new Item();
        item.setId("3");
        item.setName("Каталог");
        item.setAuthor("Оља Ивањицки");
        item.setText("1987/4");
        item.setTextUrl("http://www.mgva.rs/katalog.php?id=340");
        item.setImageUrl("http://www.mgva.rs/kat/KMGV%20Olja%20Ivanjicki,%201955-1966.jpg");
        item.setAudioUrl("http://rti.etf.bg.ac.rs/rti/ri3aor/Simulators/museum/OljaIvanjicki.mp3");
        item.setLocation(new Location(100, 40, 0));
        result.add(item);

        item = new Item();
        item.setId("4");
        item.setName("Каталог 1958-1966");
        item.setAuthor("Владимир Величковић");
        item.setText("1986/3");
        item.setTextUrl("http://www.mgva.rs/katalog.php?id=360");
        item.setImageUrl("http://www.mgva.rs/kat/KMGV%20Vladimir%20Veli%C4%8Dkovi%C4%87,%201958-1966.jpg");
        item.setVideoUrl("https://www.youtube.com/watch?v=DMIW8W2eGVY");
        item.setAudioUrl("http://rti.etf.bg.ac.rs/rti/ri3aor/Simulators/museum/VladimirVelickovic.mp3");
        item.setLocation(new Location(180, 80, 0));
        result.add(item);

        item = new Item();
        item.setId("5");
        item.setName("Каталог 1932-1939");
        item.setAuthor("Милена Павловић Барили");
        item.setText("1987/3");
        item.setTextUrl("http://www.mgva.rs/katalog.php?id=329");
        item.setImageUrl("http://www.mgva.rs/kat/KMGV%20Milena%20Pavlovi%C4%87%20Barili,%201932-1939.jpg");
        item.setVideoUrl("https://www.youtube.com/watch?v=6LI_q4TpsWk");
        item.setAudioUrl("http://rti.etf.bg.ac.rs/rti/ri3aor/Simulators/museum/MilenaPavlovicBarili.mp3");
        item.setLocation(new Location(10, 40, 0));
        result.add(item);

        item = new Item();
        item.setId("6");
        item.setName("Android camera view");
        item.setAuthor("Google inc.");
        item.setText("Android camera emulator 2018");
        item.setTextUrl("https://developer.android.com");
        item.setImageUrl("https://cdn-p.cian.site/images/2/450/075/kvartira-taganrog-smirnovskiy-pereulok-570054261-2.jpg");
        item.setVideoUrl("https://www.youtube.com/watch?v=ZaynGNXAIJ0");
        item.setAudioUrl("https://www.freesounds.info/download/couch-playin/");
        item.setLocation(new Location(220, 40, 0));
        result.add(item);

        item = new Item();
        item.setId("7");
        item.setName("Android2");
        item.setAuthor("Google inc.");
        item.setText("Android2 camera emulator 2018");
        item.setTextUrl("https://developer.android.com");
        item.setImageUrl("https://cdn-p.cian.site/images/2/450/075/kvartira-taganrog-smirnovskiy-pereulok-570054261-2.jpg");
        item.setVideoUrl("https://www.youtube.com/watch?v=ZaynGNXAIJ0");
        item.setAudioUrl("https://www.freesounds.info/download/couch-playin/");
        item.setLocation(new Location(250, 40, 3));
        result.add(item);

        return result;
    }

    public static List<FlorMap> createFlorMaps() {
        List<FlorMap> maps = new LinkedList<>();

        FlorMap florMap1 = new FlorMap();
        florMap1.setFlorNumber(0);

        Location start = new Location(0, 0, 0);
        florMap1.setStart(start);

        Location end = new Location(0, 100, 0);
        florMap1.setEnd(end);

        florMap1.setLocation(new Location(100, 50, 0));
        florMap1.setId("ETF-0");
        florMap1.setName("ETF");
        florMap1.setAuthor("");
        florMap1.setText("");
        florMap1.setTextUrl("http://etf.rs");
        florMap1.setImageUrl("http://rti.etf.bg.ac.rs/rti/ri3aor/Simulators/museum/ETF-prizemlje-upis.jpg");
        String extra = "https://www.etf.bg.ac.rs/uploads/files/strane/fakultet/slike_fakulteta/zgrada_izunutra1.jpg, https://upload.wikimedia.org/wikipedia/commons/a/a6/Fakultet_elektrotehnicki_u_beogradu_ulaz.jpg";
        florMap1.setAdditionalImagesUrls(extra);
        maps.add(florMap1);

        FlorMap florMap2 = new FlorMap();
        florMap2.setFlorNumber(0);

        start = new Location(0, 0, 0);
        florMap2.setStart(start);

        end = new Location(0, 100, 3);
        florMap2.setEnd(end);

        florMap2.setLocation(new Location(100, 50, 3));
        florMap2.setId("ETF-1");
        florMap2.setName("ETF-1");
        florMap2.setAuthor("");
        florMap2.setText("");
        florMap2.setTextUrl("http://etf.rs");
        florMap2.setImageUrl("http://rti.etf.bg.ac.rs/rti/ri3aor/Simulators/museum/ETF-prizemlje-upis.jpg");
        extra = "https://www.etf.bg.ac.rs/uploads/files/strane/fakultet/slike_fakulteta/zgrada_izunutra1.jpg, https://upload.wikimedia.org/wikipedia/commons/a/a6/Fakultet_elektrotehnicki_u_beogradu_ulaz.jpg";
        florMap2.setAdditionalImagesUrls(extra);
        maps.add(florMap2);

        return maps;
    }

    public static MuseumMap createMuseumMap() {
        MuseumMap result = new MuseumMap();

        List<Item> items = createItems();

        FlorMap flor = createFlorMaps().get(0);

        result.setItems(items);
        result.setFlorMap(flor);

        return result;
    }

    public static Museum createMuseum() {
        Museum museum = new Museum();

        List<Item> items = createItems();
        museum.setItems(items);

        List<FlorMap> maps = createFlorMaps();
        museum.setMaps(maps);

        MuseumMap mMap = new MuseumMap();
        mMap.setFlorMap(maps.get(0));
        mMap.setItems(items);

        List<Exhibition> exhibitions = new LinkedList<>();

        Exhibition exhibition = getExhibition(items, mMap, 0);
        exhibitions.add(exhibition);
        exhibition = getExhibition(items, mMap, 1);
        exhibitions.add(exhibition);
        exhibition = getExhibition(items, mMap, 2);
        exhibitions.add(exhibition);

        museum.setExhibitions(exhibitions);

        museum.setMuseumMaps(createMuseumMaps());

        List<Room> rooms = createRooms(items, maps);
        museum.setRooms(rooms);

        return museum;
    }

    private static Exhibition getExhibition(List<Item> items, MuseumMap mMap, int index) {
        Exhibition exhibition = new Exhibition();
        exhibition.setItems(items);
        exhibition.load(items.get(index));
        List<MuseumMap> mMaps = new LinkedList<>();
        mMaps.add(mMap);
        exhibition.setMuseumMaps(mMaps);
        return exhibition;
    }

    public static List<MuseumMap> createMuseumMaps() {
        List<MuseumMap> maps = new LinkedList<>();

        List<Item> items = createItems();

        FlorMap florMap = createFlorMaps().get(0);

        for (Item item : items) {
            MuseumMap mMap = new MuseumMap();
            mMap.setFlorMap(florMap);
            mMap.setItems(items);
            mMap.setText(item.getText());
            mMap.setAuthor(item.getAuthor());
            mMap.setName(item.getName());
            mMap.setImageUrl(item.getImageUrl());
            mMap.setLocation(item.getLocation());

            maps.add(mMap);
        }

        return maps;
    }

    public static List<Room> createRooms(List<Item> items, List<FlorMap> maps) {
        List<Room> rooms = new LinkedList<>();

        Room r0 = new Room();
        Room r1 = new Room();
        Room r2 = new Room();
        Room r3 = new Room();
        Room r4 = new Room();
        Room r5 = new Staircase();

        r0.setId("0");
        r0.setName("Атријум");
        r0.setText("Улаз у музеј");
        r0.setMap(maps.get(0));
        List<Room> r0s = new LinkedList<>();
        r0s.add(r1);
        r0s.add(r2);
        r0.setConnected(r0s);
        rooms.add(r0);

        r1.setId("1");
        r1.setName("Главна сала");
        r1.setText("Основна поставка музеја");
        r1.setMap(maps.get(0));
        List<Room> r1s = new LinkedList<>();
        r1s.add(r0);
        r1s.add(r5);
        r1.setConnected(r1s);
        List<Item> i1 = new LinkedList<>();
        i1.add(items.get(0));
        i1.add(items.get(1));
        i1.add(items.get(2));
        r1.setItems(i1);
        rooms.add(r1);

        r2.setId("2");
        r2.setName("Сала 2");
        r2.setText("Легати");
        r2.setMap(maps.get(0));
        List<Room> r2s = new LinkedList<>();
        r2s.add(r0);
        r2s.add(r3);
        r2.setConnected(r2s);
        List<Item> i2 = new LinkedList<>();
        i2.add(items.get(3));
        i2.add(items.get(4));
        r2.setItems(i2);
        rooms.add(r2);

        r3.setId("3");
        r3.setName("Сала 3");
        r3.setText("Мала сала");
        r3.setMap(maps.get(0));
        List<Room> r3s = new LinkedList<>();
        r3s.add(r2);
        r3.setConnected(r3s);
        List<Item> i3 = new LinkedList<>();
        i3.add(items.get(5));
        r3.setItems(i3);
        rooms.add(r3);

        r4.setId("4");
        r4.setName("Сала 4");
        r4.setText("Легат Љубе Поповића");
        r4.setMap(maps.get(1));
        List<Room> r4s = new LinkedList<>();
        r4s.add(r5);
        r4.setConnected(r4s);
        List<Item> i4 = new LinkedList<>();
        i4.add(items.get(6));
        r4.setItems(i4);
        rooms.add(r4);

        r5.setId("5");
        r5.setName("Степениште");
        r5.setText("Степенице");
        r5.setMap(maps.get(0));
        List<Room> r5s = new LinkedList<>();
        r5s.add(r1);
        r5s.add(r4);
        r5.setConnected(r5s);
        List<Item> i5 = new LinkedList<>();
        r5.setItems(i5);
        rooms.add(r5);

        return rooms;
    }

}
