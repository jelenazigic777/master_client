package rs.etf.navigation.db;

import android.content.Context;
import android.os.AsyncTask;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class HttpGetRequestArray extends AsyncTask<String, Void, JSONArray> {

    private Context context;
    private JSONArray jsonArray;
    private HttpGetRequestArray.onResponse onResponse;

    public HttpGetRequestArray.onResponse getOnResponse() {
        return onResponse;
    }

    public HttpGetRequestArray(Context context) {
        this.context = context;
    }

    public void setOnResponse(HttpGetRequestArray.onResponse onResponse) {
        this.onResponse = onResponse;
    }

    @Override
    protected JSONArray doInBackground(String... params) {
        // TODO Auto-generated method stub
        try {
            HttpGet get = new HttpGet(params[0]);
            HttpClient client = new DefaultHttpClient();

            HttpResponse response = client.execute(get);
            HttpEntity entity = response.getEntity();
            String result = EntityUtils.toString(entity);
            jsonArray = new JSONArray(result);
        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return jsonArray;
    }

    @Override
    protected void onPostExecute(JSONArray result) {
        // TODO Auto-generated method stub
        super.onPostExecute(result);
       // this.onResponse.onResponse(result);
    }

    public interface onResponse {
        public void onResponse(JSONArray object);
    }

}
