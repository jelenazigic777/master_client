package rs.etf.navigation.db;


import android.content.Context;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import rs.etf.navigation.location.Location;
import rs.etf.navigation.place.Exhibition;
import rs.etf.navigation.place.FlorMap;
import rs.etf.navigation.place.Item;
import rs.etf.navigation.place.Museum;
import rs.etf.navigation.place.MuseumMap;
import rs.etf.navigation.place.Room;
import rs.etf.navigation.place.Staircase;

public class FileLoader {


    public static Museum createMuseum(Context context, int items, int connections) {
        List<String[]> arg1 = parseFile(context, items);
        Map<String, Item> allItems = createItems(arg1);

        List<String[]> arg2 = parseFile(context, connections);
        createAllConnections(allItems, arg2);

        for (Item item : allItems.values()) {
            if (item instanceof Museum) return (Museum) item;
        }
        return null;
    }

    public static List<String[]> parseFile(Context context, int file) {
        List<String[]> result = new LinkedList<>();
        try (InputStream in = context.getResources().openRawResource(file);) {
            BufferedReader br = new BufferedReader(new InputStreamReader(in));

            String s;
            while ((s = br.readLine()) != null) {
                s = s + "; ";
                String[] arg = s.split(";");
                result.add(arg);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static Map<String, Item> createItems(List<String[]> args) {
        Map<String, Item> result = new HashMap<>();

        for (String[] arg : args) {
            Item item = createItem(arg);
            result.put(item.getId(), item);
        }
        return result;

    }

    public static void createAllConnections(Map<String, Item> items, List<String[]> args) {
        for (String[] arg : args) {
            createConnections(items, arg);
        }


    }

    public static Item createItem(String[] args) {
        Item item = null;
        int type = Integer.parseInt(args[2]);
        switch (type) {
            case Item.MUSEUM:
                item = new Museum();
                break;
            case Item.ROOM:
                item = new Room();
                break;
            case Item.STAIRCASE_UP:
                item = new Staircase();
                break;
            case Item.STAIRCASE_DOWN:
                item = new Staircase();
                break;
            case Item.MAP:
                item = new FlorMap();
                int fNumber = Integer.parseInt(args[24]);
                Location start = createLocation(args[25], args[26], args[27]);
                Location end = createLocation(args[28], args[29], args[30]);
                ((FlorMap) item).setFlorNumber(fNumber);
                ((FlorMap) item).setStart(start);
                ((FlorMap) item).setEnd(end);
                break;
            case Item.EXHIBITION:
                item = new Exhibition();
                break;
            case Item.PICTURE:
                item = new Item();
                break;
            case Item.MUSEUM_MAP:
                item = new MuseumMap();
                break;
        }
        item.setId(args[0]);
        item.setInstitution(args[1]);
        item.setType(Integer.parseInt(args[2]));
        item.setLanguage(args[3]);
        item.setName(args[4]);
        item.setAuthor(args[5]);
        item.setText(args[6]);
        item.setTextUrl(args[7]);
        item.setTextUrlLocal(args[8]);
        item.setImageUrl(args[9]);
        item.setImageUrlLocal(args[10]);
        item.setAudioUrl(args[11]);
        item.setAudioUrlLocal(args[12]);
        if (args[13] != null && !args[13].equals("")) {
            item.setAudioType(Integer.parseInt(args[13]));
        }
        item.setVideoUrl(args[14]);
        item.setVideoUrlLocal(args[15]);
        item.setLocation(createLocation(args[16], args[17], args[18]));
        item.setRadius(Double.parseDouble(args[19]));
        item.setRoom(args[20]);
        item.setAdditionalImagesUrls(args[21]);
        item.setAdditionalImagesUrlsLocal(args[22]);
        item.setQr(args[23]);

        return item;
    }

    private static Location createLocation(String argX, String argY, String argZ) {
        Double x = Double.parseDouble(argX);
        Double y = Double.parseDouble(argY);
        Double z = Double.parseDouble(argZ);
        Location location = new Location(x, y, z);
        return location;
    }

    public static void createConnections(Map<String, Item> items, String[] args) {
        int type1 = Integer.parseInt(args[1]);
        switch (type1) {
            case Item.MUSEUM:
                connectMuseum(items, args);
                break;
            case Item.ROOM:
                connectRooms(items, args);
                break;
            case Item.MUSEUM_MAP:
                connectMuseumMaps(items, args);

                break;
            case Item.EXHIBITION:
                connectExhibitions(items, args);

                break;
        }
    }

    private static void connectRooms(Map<String, Item> items, String[] args) {
        Room room1 = (Room) items.get(args[2]);
        int type2 = Integer.parseInt(args[3]);
        switch (type2) {
            case Item.MAP:
                FlorMap fMap = (FlorMap) items.get(args[4]);
                room1.setMap(fMap);
                break;

            case Item.PICTURE:
                Item item = items.get(args[4]);
                List<Item> mItems = room1.getItems();
                if (mItems == null) {
                    mItems = new LinkedList<>();
                    room1.setItems(mItems);
                }
                mItems.add(item);
                break;
            case Item.ROOM:
                Room room = (Room) items.get(args[4]);
                List<Room> rooms = room1.getConnected();
                if (rooms == null) {
                    rooms = new LinkedList<>();
                    room1.setConnected(rooms);
                }
                rooms.add(room);
                break;

        }
    }

    private static void connectMuseumMaps(Map<String, Item> items, String[] args) {
        MuseumMap museumMap = (MuseumMap) items.get(args[2]);
        int type2 = Integer.parseInt(args[3]);
        switch (type2) {
            case Item.PICTURE:
                Item item = items.get(args[4]);
                List<Item> mItems = museumMap.getItems();
                if (mItems == null) {
                    mItems = new LinkedList<>();
                    museumMap.setItems(mItems);
                }
                mItems.add(item);
                break;

            case Item.MAP:
                FlorMap map = (FlorMap) items.get(args[4]);
                museumMap.setFlorMap(map);
                break;
        }
    }

    private static void connectExhibitions(Map<String, Item> items, String[] args) {
        Exhibition exhibition = (Exhibition) items.get(args[2]);
        int type2 = Integer.parseInt(args[3]);
        switch (type2) {
            case Item.PICTURE:
                Item item = items.get(args[4]);
                List<Item> mItems = exhibition.getItems();
                if (mItems == null) {
                    mItems = new LinkedList<>();
                    exhibition.setItems(mItems);
                }
                mItems.add(item);
                break;

            case Item.MUSEUM_MAP:
                MuseumMap map = (MuseumMap) items.get(args[4]);
                List<MuseumMap> flors = exhibition.getMuseumMaps();
                if (flors == null) {
                    flors = new LinkedList<>();
                    exhibition.setMuseumMaps(flors);
                }
                flors.add(map);
                break;
        }
    }

    private static void connectMuseum(Map<String, Item> items, String[] args) {
        String key1 = args[2];
        Museum museum = (Museum) items.get(args[2]);
        int type2 = Integer.parseInt(args[3]);
        switch (type2) {
            case Item.PICTURE:
                Item item = items.get(args[4]);
                List<Item> mItems = museum.getItems();
                if (mItems == null) {
                    mItems = new LinkedList<>();
                    museum.setItems(mItems);
                }
                mItems.add(item);
                break;

            case Item.MAP:
                FlorMap map = (FlorMap) items.get(args[4]);
                List<FlorMap> flors = museum.getMaps();
                if (flors == null) {
                    flors = new LinkedList<>();
                    museum.setMaps(flors);
                }
                flors.add(map);
                break;
            case Item.EXHIBITION:
                Exhibition exhibition = (Exhibition) items.get(args[4]);
                List<Exhibition> exhibitions = museum.getExhibitions();
                if (exhibitions == null) {
                    exhibitions = new LinkedList<>();
                    museum.setExhibitions(exhibitions);
                }
                exhibitions.add(exhibition);
                break;
            case Item.ROOM:
                Room room = (Room) items.get(args[4]);
                List<Room> rooms = museum.getRooms();
                if (rooms == null) {
                    rooms = new LinkedList<>();
                    museum.setRooms(rooms);
                }
                rooms.add(room);
                break;
            case Item.MUSEUM_MAP:
                MuseumMap museumMap = (MuseumMap) items.get(args[4]);
                List<MuseumMap> museumMaps = museum.getMuseumMaps();
                if (museumMaps == null) {
                    museumMaps = new LinkedList<>();
                    museum.setMuseumMaps(museumMaps);
                }
                museumMaps.add(museumMap);
                break;

        }
    }

}
