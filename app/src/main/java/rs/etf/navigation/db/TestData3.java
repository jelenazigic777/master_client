package rs.etf.navigation.db;

import rs.etf.navigation.gui.TestActivity;
import rs.etf.navigation.place.Museum;

public class TestData3 {

    public static Museum createMuseum() {
        Museum result = JsonLoader.createMuseum(TestActivity.context);
        return result;
    }
}
