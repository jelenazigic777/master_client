package rs.etf.navigation.db;

import android.content.Context;

import java.util.LinkedList;
import java.util.List;

import rs.etf.navigation.R;
import rs.etf.navigation.gui.TestActivity;
import rs.etf.navigation.location.Location;
import rs.etf.navigation.place.Exhibition;
import rs.etf.navigation.place.FlorMap;
import rs.etf.navigation.place.Item;
import rs.etf.navigation.place.Museum;
import rs.etf.navigation.place.MuseumMap;
import rs.etf.navigation.place.Room;
import rs.etf.navigation.place.Staircase;

public class TestData2 {

    public static Museum createMuseum() {
        Museum result = FileLoader.createMuseum(TestActivity.context, R.raw.items, R.raw.connections);
        return result;
    }
}
